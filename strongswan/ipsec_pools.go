package strongswan

import (
	"fmt"
	"io"
	"strconv"
	"strings"
)

// IpsecPoolsReader is type to Read slice of Pools
// with `ipsec statusall` format
type IpsecPoolsReader struct {
	buf [2048]byte
}

func (ipr *IpsecPoolsReader) Read(r io.Reader) ([]Pool, error) {
	n, err := r.Read(ipr.buf[:])
	if err != nil && err != io.EOF {
		return nil, err
	}
	splitted := strings.Split(string(ipr.buf[:n]), "Virtual IP pools (size/online/offline):\n  ")
	if len(splitted) == 1 {
		return make([]Pool, 0), nil
	}
	splitted = strings.Split(splitted[1], "Listening IP addresses:")
	poolsLines := strings.Split(splitted[0], "\n")
	poolsLines = poolsLines[:len(poolsLines)-1]
	pools := make([]Pool, len(poolsLines))
	for i, poolStr := range poolsLines {
		splitted = strings.Split(poolStr, ": ")
		pools[i].Addr = splitted[0]
		splitted = strings.Split(splitted[1], "/")
		size, err := strconv.Atoi(splitted[0])
		if err != nil {
			return nil, fmt.Errorf("Cannot parse pool size %s: %s", splitted[0], err)
		}
		pools[i].Size = size
		on, err := strconv.Atoi(splitted[1])
		if err != nil {
			return nil, fmt.Errorf("Cannot parse pool online %s: %s", splitted[1], err)
		}
		pools[i].On = on
		off, err := strconv.Atoi(splitted[2])
		if err != nil {
			return nil, fmt.Errorf("Cannot parse pool offline %s: %s", splitted[2], err)
		}
		pools[i].Off = off
	}
	return pools, nil
}
