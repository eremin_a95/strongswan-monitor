package strongswan

import (
	"fmt"
	"io"
	"strconv"
	"time"

	vici "github.com/strongswan/govici"
)

// ViciConnReader is ConnectionsReader which reads from vici socket
type ViciConnReader struct {
	session *vici.Session
	msg     *vici.Message
}

// ViciOptions is struct representing vici message
// sending to vici socket
type ViciOptions struct {
	IKE string `vici:"ike"`
}

// NewViciConnReader creates new ViciConnReader
func NewViciConnReader() (*ViciConnReader, error) {
	session, err := vici.NewSession()
	if err != nil {
		return nil, err
	}
	opts := ViciOptions{}
	msg, err := vici.MarshalMessage(&opts)
	if err != nil {
		return nil, err
	}
	return &ViciConnReader{session, msg}, nil
}

func fillConnFromViciMsg(conn *Connection, msg *vici.Message) {
	conn.LocalAuth = make([]AuthType, 0, 2)
	conn.RemoteAuth = make([]AuthType, 0, 2)
	conn.LocalID = make([]string, 0, 2)
	conn.RemoteID = make([]string, 0, 2)
	conn.LocalAddr = msg.Get("local_addrs").([]string)
	conn.RemoteAddr = msg.Get("remote_addrs").([]string)
	conn.Version = ParseIKEVersion(msg.Get("version").(string))

	reauth, _ := strconv.Atoi(msg.Get("reauth_time").(string))
	submsg := msg.Get("children").(*vici.Message)
	conn.Children = make([]Child, len(submsg.Keys()))
	for i, key := range submsg.Keys() {
		subsubmsg := submsg.Get(key).(*vici.Message)
		conn.Children[i].Mode = ParseConnectionMode(subsubmsg.Get("mode").(string))
		rekey, _ := strconv.Atoi(subsubmsg.Get("mode").(string))
		conn.Children[i].Rekeying = time.Duration(rekey)
		conn.Children[i].Reauth = time.Duration(reauth)
		conn.Children[i].Local = subsubmsg.Get("local-ts").([]string)
		conn.Children[i].Remote = subsubmsg.Get("remote-ts").([]string)
	}

	if msg.Get("local-1") != nil {
		submsg = msg.Get("local-1").(*vici.Message)
		conn.LocalAuth = append(conn.LocalAuth, ParseAuthType(submsg.Get("class").(string)))
		if submsg.Get("id") != nil {
			conn.LocalID = append(conn.LocalID, submsg.Get("id").(string))
		}
	}

	if msg.Get("remote-1") != nil {
		submsg = msg.Get("remote-1").(*vici.Message)
		conn.RemoteAuth = append(conn.RemoteAuth, ParseAuthType(submsg.Get("class").(string)))
		if submsg.Get("id") != nil {
			conn.RemoteID = append(conn.RemoteID, submsg.Get("id").(string))
		}
	}

	if msg.Get("local-2") != nil {
		submsg = msg.Get("local-1").(*vici.Message)
		conn.LocalAuth = append(conn.LocalAuth, ParseAuthType(submsg.Get("class").(string)))
		if submsg.Get("id") != nil {
			conn.LocalID = append(conn.LocalID, submsg.Get("id").(string))
		}
	}

	if msg.Get("remote-2") != nil {
		submsg = msg.Get("remote-2").(*vici.Message)
		conn.RemoteAuth = append(conn.RemoteAuth, ParseAuthType(submsg.Get("class").(string)))
		if submsg.Get("id") != nil {
			conn.RemoteID = append(conn.RemoteID, submsg.Get("id").(string))
		}
	}
}

func fillSAFromViciMsg(sa *SA, msg *vici.Message) {
	switch msg.Get("state").(string) {
	case "ESTABLISHED":
		sa.IKESA.Established = true
	default:
		//TODO
		return
	}
	sa.IKESA.Version = ParseIKEVersion(msg.Get("version").(string))
	sa.IKESA.LocalAddr = msg.Get("local-host").(string) + ":" + msg.Get("local-port").(string)
	if msg.Get("nat-local") != nil && msg.Get("nat-local").(string) == "yes" {
		sa.IKESA.LocalAddr += " (NAT)"
	}
	sa.IKESA.LocalID = msg.Get("local-id").(string)
	sa.IKESA.RemoteAddr = msg.Get("remote-host").(string) + ":" + msg.Get("remote-port").(string)
	if msg.Get("nat-remote") != nil && msg.Get("nat-remote").(string) == "yes" {
		sa.IKESA.RemoteAddr += " (NAT)"
	}
	sa.IKESA.RemoteID = msg.Get("remote-id").(string)
	sa.IKESA.InitiatorSPI = msg.Get("initiator-spi").(string)
	sa.IKESA.ResponderSPI = msg.Get("responder-spi").(string)
	if msg.Get("initiator") != nil && msg.Get("initiator").(string) == "yes" {
		sa.IKESA.IsInitiator = true
	}
	encr := msg.Get("encr-alg").(string)
	if keysize := msg.Get("encr-keysize"); keysize != nil {
		encr += "_" + keysize.(string)
	}
	integ := msg.Get("integ-alg").(string)
	if keysize := msg.Get("integ-keysize"); keysize != nil {
		integ += "_" + keysize.(string)
	}
	sa.IKESA.Security = fmt.Sprintf("%s/%s/%s/%s", encr, integ, msg.Get("prf-alg"), msg.Get("dh-group"))
	sa.IKESA.Alive = msg.Get("established").(string) + " seconds"
	sa.IKESA.UntilReauth = msg.Get("reauth-time").(string) + " seconds"
	//TODO: maybe handle virtual IPs?
	children := msg.Get("child-sas").(*vici.Message)
	//TODO: handle with others children SAs
	if len(children.Keys()) == 0 {
		return
	}
	sa.ChildSAs = make([]ChildSA, len(children.Keys()))
	for i, key := range children.Keys() {
		child := children.Get(key).(*vici.Message)
		switch child.Get("state").(string) {
		case "INSTALLED":
			sa.ChildSAs[i].Installed = true
		default:
			//TODO
			return
		}
		sa.ChildSAs[i].ReqID, _ = strconv.ParseInt(child.Get("reqid").(string), 10, 64)
		sa.ChildSAs[i].Mode = ParseConnectionMode(child.Get("mode").(string))
		sa.ChildSAs[i].Proto = ParseIPsecProto(child.Get("protocol").(string))
		if child.Get("encap") != nil && child.Get("encap").(string) == "yes" {
			sa.ChildSAs[i].UDPEncaps = true
		}
		sa.ChildSAs[i].InputSPI = child.Get("spi-in").(string)
		sa.ChildSAs[i].OutputSPI = child.Get("spi-out").(string)
		encr = child.Get("encr-alg").(string)
		if keysize := child.Get("encr-keysize"); keysize != nil {
			encr += "_" + keysize.(string)
		}
		integ = child.Get("integ-alg").(string)
		if keysize := child.Get("integ-keysize"); keysize != nil {
			integ += "_" + keysize.(string)
		}
		prf := ""
		if prfa := child.Get("prf-alg"); prfa != nil {
			prf = "/" + prfa.(string)
		}
		dh := ""
		if dhg := child.Get("dh-group"); dhg != nil {
			dh = "/" + dhg.(string)
		}
		sa.ChildSAs[i].Security = fmt.Sprintf("%s/%s%s%s", encr, integ, prf, dh)
		bIn, _ := strconv.ParseInt(child.Get("bytes-in").(string), 10, 64)
		sa.ChildSAs[i].BytesIn = ByteNum(bIn)
		sa.ChildSAs[i].PktsIn, _ = strconv.ParseUint(child.Get("packets-in").(string), 10, 64)
		if useIn := child.Get("use-in"); useIn != nil {
			sa.ChildSAs[i].LastInAgo = useIn.(string) + " seconds"
		}
		bOut, _ := strconv.ParseInt(child.Get("bytes-out").(string), 10, 64)
		sa.ChildSAs[i].BytesOut = ByteNum(bOut)
		sa.ChildSAs[i].PktsOut, _ = strconv.ParseUint(child.Get("packets-out").(string), 10, 64)
		if useOut := child.Get("use-out"); useOut != nil {
			sa.ChildSAs[i].LastOutAgo = useOut.(string) + " seconds"
		}
		sa.ChildSAs[i].UntilRekey = child.Get("rekey-time").(string) + " seconds"
		localTS := child.Get("local-ts").([]string)
		for j, ts := range localTS {
			if j != 0 {
				sa.ChildSAs[i].LocalNet += ","
			}
			sa.ChildSAs[i].LocalNet += ts
		}
		remoteTS := child.Get("remote-ts").([]string)
		for j, ts := range remoteTS {
			if j != 0 {
				sa.ChildSAs[i].RemoteNet += ","
			}
			sa.ChildSAs[i].RemoteNet += ts
		}
	}
}

func (vcr *ViciConnReader) Read(io.Reader) ([]*Connection, error) {
	ms, err := vcr.session.StreamedCommandRequest("list-conns", "list-conn", vcr.msg)
	if err != nil {
		return nil, err
	}
	conns := make([]*Connection, 0, len(ms.Messages())-1)
	for i, m := range ms.Messages()[:len(ms.Messages())-1] {
		conns = append(conns, new(Connection))
		for _, key := range m.Keys() {
			conns[i].Name = key
			conns[i].SAs = make([]*SA, 0, 4)
			submsg := m.Get(key).(*vici.Message)
			fillConnFromViciMsg(conns[i], submsg)
		}
	}
	ms, err = vcr.session.StreamedCommandRequest("list-sas", "list-sa", vcr.msg)
	if err != nil {
		return nil, err
	}
	for _, m := range ms.Messages()[:len(ms.Messages())-1] {
		name := m.Keys()[0]
		for _, conn := range conns {
			if conn.Name == name {
				sa := new(SA)
				sa.IKESA.Auth = conn.RemoteAuth[0]
				conn.SAs = append(conn.SAs, sa)
				fillSAFromViciMsg(sa, m.Get(name).(*vici.Message))
				break
			}
		}
	}
	return conns, nil
}
