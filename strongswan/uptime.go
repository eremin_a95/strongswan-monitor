package strongswan

import (
	"bytes"
	"fmt"
	"io"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

// Summary is struct to store summary strongswan daemon info
type Summary struct {
	Uptime                      string
	Since                       time.Time
	SATotal, SAHalfOpened       int
	TotalBytesIn, TotalBytesOut int64
	TotalPktsIn, TotalPktsOut   int64
}

func (s *Summary) String() string {
	return fmt.Sprintf("Uptime: %v (since %s)\nIKE SAs: %v total, %v connecting\n", s.Uptime, s.Since.Format("15:04:05 02 Jan 2006"), s.SATotal, s.SAHalfOpened)
}

// ParseDuration is function to parse string to time.Duration
func ParseDuration(str string) (time.Duration, error) {
	splitted := strings.Split(str, " ")
	value, err := strconv.Atoi(splitted[0])
	if err != nil {
		return 0, fmt.Errorf("Error parsing value %s: %s", splitted[0], err)
	}
	switch splitted[1] {
	case "seconds", "second", "s":
		return time.Duration(value) * time.Second, nil
	case "minutes", "minute", "m":
		return time.Duration(value) * time.Minute, nil
	case "hours", "hour", "h":
		return time.Duration(value) * time.Hour, nil
	case "days", "day", "d":
		return time.Duration(value) * time.Hour * 24, nil
	default:
		return 0, fmt.Errorf("Unknown units: %s", splitted[1])
	}
}

// UptimeReader is interface for types which
// can read Summary struct from io.Reader
type UptimeReader interface {
	Read(io.Reader) (time.Duration, time.Time, error)
}

// UptimeGetter is a struct to handle with Uptime getting
type UptimeGetter struct {
	getUptime func(UptimeReader) (time.Duration, time.Time, error)
	reader    UptimeReader
}

// GetUptime is function which returns uptime and lifetime or error
func (ug *UptimeGetter) GetUptime() (time.Duration, time.Time, error) {
	return ug.getUptime(ug.reader)
}

// NewUptimeGetter return instance of UptimeGetter or error
// if it is not possible to init any UptimeReader
func NewUptimeGetter() (*UptimeGetter, error) {
	var reader UptimeReader
	var getUptime func(UptimeReader) (time.Duration, time.Time, error)
	reader = new(IpsecSummaryReader)
	getUptime = func(reader UptimeReader) (time.Duration, time.Time, error) {
		out, err := exec.Command("ipsec", "statusall").Output()
		if err != nil {
			return 0, time.Time{}, fmt.Errorf("Cannot exec \"ipsec statusall\" command: %s", err)
		}
		uptime, since, err := reader.Read(bytes.NewReader(out))
		if err != nil {
			return 0, time.Time{}, fmt.Errorf("Cannot read uptime by IpsecSummaryReader: %s", err)
		}
		return uptime, since, nil
	}
	if _, _, err := getUptime(reader); err == nil {
		getter := new(UptimeGetter)
		getter.reader = reader
		getter.getUptime = getUptime
		return getter, nil
	}
	return nil, fmt.Errorf("Cannot initialize any UptimeReader")
}
