package strongswan

import (
	"bytes"
	"testing"
	"time"
)

const SwanctlStatsExample1 = "uptime: 2 seconds, since May 20 10:01:36 2019\nworker threads: 16 total, 11 idle, working: 4/0/1/0\njob queues: 0/0/0/0\njobs scheduled: 6\nIKE_SAs: 2 total, 0 half-open\nmallinfo: sbrk 3002368, mmap 0, used 2054272, free 948096\nloaded plugins: charon-systemd random nonce aes sha1 sha2 hmac pem pkcs1 x509 revocation curve25519 gmp curl kernel-netlink socket-default updown vici\nencryption:\n  AES_CBC[aes]\nintegrity:\n  HMAC_SHA1_96[hmac]..."

func TestSwanctlSummaryReader(t *testing.T) {
	reader := bytes.NewBufferString(SwanctlStatsExample1)
	ssr := &SwanctlSummaryReader{}
	uptime, since, err := ssr.Read(reader)
	if err != nil {
		t.Error("For err expected", nil, "returned", err)
		t.FailNow()
	}
	if uptime != "2 seconds" {
		t.Error("For sum.Uptime expected 2 seconds returned", uptime)
	}
	testDate := time.Date(2019, time.May, 20, 10, 01, 36, 0, &time.Location{})
	if since.Sub(testDate) > time.Second || since.Sub(testDate) < (-time.Second) {
		t.Error("For sum.Since expected", testDate, "returned", since)
	}
	/*if sum.SATotal != 2 {
		t.Error("For sum.SATotal expected", 2, "returned", sum.SATotal)
	}
	if sum.SAHalfOpened != 0 {
		t.Error("For sum.SAHalfOpened expected", 0, "returned", sum.SAHalfOpened)
	}*/
}
