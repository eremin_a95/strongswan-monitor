# strongswan-monitor

[![GoDoc](https://godoc.org/gitlab.com/eremin_a95/sessions?status.svg)](https://godoc.org/gitlab.com/eremin_a95/strongswan-monitor/strongswan)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/eremin_a95/strongswan-monitor)](https://goreportcard.com/report/gitlab.com/eremin_a95/strongswan-monitor)

Terminal GUI for monitoring [strongswan](https://www.strongswan.org/)
IKE daemon.

Based on [termui](https://github.com/gizak/termui) library.

There are several tabs which provide access to different information
about daemon.

You can switch tabs with `Tab`, `Space` (forward) or
`Ctrl` + `Space`, `Backspace` (backward) buttons.
You can also choose tabs directly by `1` - `9` buttons
(`1` - `5` if there is 5 tabs).

To close app press `q` or `Ctrl`+`c`.

## Config

Example of config file (this configuration is being used by default):

```yaml
tabs:
- stats
- conns
- sas
- pools
- certs
conns: 2000
pools: 4000
summary: 4000
certs: 8000
```

`tabs` list contains types of tabs which are placed on the screen:
- `stats` for [Statistics](#statistics) panel
- `conns` for [Connections](#connections) panel
- `sas` for [SAs](#sas) panel
- `pools` for [Pools](#pools) panel
- `certs` for [Certs](#certs) panel

`conns` keyword is for setting update period of connections (including SAs) info
in milliseconds

`pools` keyword is for setting update period of pools info in milliseconds

`summary` keyword is for setting update period of summary info in milliseconds

`certs` keyword is for setting update period of certs info in milliseconds

## Tabs

### Statistics

Statistics tab shows daemon's uptime info, number of Security Associations and
statistics of I/O bytes/packets.

I/O bytes and packets chart is autoscaling and updates every 5 seconds.

![](images/Statistics.png)

### Connections

Connections tab shows list of connections which daemon is working with.
Every node of list contents info about parameters of connection

You can move through tree by `Up`/`Down` arrows, expand or collapse node by
`e` (**e**xpand), `c` (**c**ollapse) or `Enter` (toggle), expand whole tree by
`Shift`+`E`, collapse whole tree by `Shift`+`C`

![](images/Connections.png)

### SAs

SAs tab shows list of connections where every node contents list of connected
peer which installed Security Association with.

Control shortcuts is same as for [Connections](#connections) tab.

![](images/SAs.png)

### Pools

Pools tab shows info about virtual IP addresses pools.

![](images/Pools.png)


### Certs

Certs tab shows info about certificates which daemon works with.

Control shortcuts is same as for [Connections](#connections) tab.

![](images/Certs.png)