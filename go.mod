module gitlab.com/eremin_a95/strongswan-monitor

go 1.13

require (
	github.com/gizak/termui/v3 v3.1.0
	github.com/strongswan/govici v0.0.0-20191006202942-f12abd5e3d2b
	gopkg.in/yaml.v2 v2.2.4
)
