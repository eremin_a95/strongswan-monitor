package strongswan

import (
	"crypto/x509"
	"io"

	vici "github.com/strongswan/govici"
)

// ViciCertsReader is CertReader which reads from vici socket
type ViciCertsReader struct {
	session *vici.Session
	msg     *vici.Message
}

// NewViciCertsReader creates new ViciCertsReader
func NewViciCertsReader() (*ViciCertsReader, error) {
	session, err := vici.NewSession()
	if err != nil {
		return nil, err
	}
	opts := ViciOptions{}
	msg, err := vici.MarshalMessage(&opts)
	if err != nil {
		return nil, err
	}
	return &ViciCertsReader{session, msg}, nil
}

func isAllNil(certs []*Cert) bool {
	for i := range certs {
		if certs[i] != nil {
			return false
		}
	}
	return true
}

func (vcr *ViciCertsReader) Read(io.Reader) ([]*Cert, error) {
	ms, err := vcr.session.StreamedCommandRequest("list-certs", "list-cert", vcr.msg)
	if err != nil {
		return nil, err
	}
	certs := make([]*Cert, 0, len(ms.Messages())-1)
	for i, m := range ms.Messages()[:len(ms.Messages())-1] {
		certs = append(certs, new(Cert))
		cert, err := x509.ParseCertificate([]byte(m.Get("data").(string)))
		if err != nil {
			return nil, err
		}
		certs[i].Cert = cert
	}
	cas := make([]*Cert, 0, 4)
	for i, cert := range certs {
		if cert.Cert.IsCA {
			cas = append(cas, cert)
			certs[i] = nil
		}
	}
	issuers := cas
	for !isAllNil(certs) {
		for _, issuer := range issuers {
			for i := range certs {
				if certs[i] != nil {
					if (certs[i].Cert.Issuer.String() == issuer.Cert.Subject.String()) && (certs[i].Cert.Issuer.SerialNumber == issuer.Cert.Subject.SerialNumber) {
						if issuer.Issued == nil {
							issuer.Issued = make([]*Cert, 0, 4)
						}
						issuer.Issued = append(issuer.Issued, certs[i])
						certs[i].Issuer = issuer
						certs[i] = nil
					}
				}
			}
		}
		newIssuers := make([]*Cert, 0, len(issuers))
		for _, issuer := range issuers {
			for _, subIssuer := range issuer.Issued {
				newIssuers = append(newIssuers, subIssuer)
			}
		}
		issuers = newIssuers
	}
	return cas, nil
}
