package strongswan

import (
	"fmt"
	"io"
	"strings"
	"time"
)

// IpsecSummaryReader is type to Read Summary struct
// with `ipsec statusall` format
type IpsecSummaryReader struct {
	buf [2048]byte
}

func (s *IpsecSummaryReader) Read(r io.Reader) (time.Duration, time.Time, error) {
	//sum := Summary{}
	n, err := r.Read(s.buf[:])
	if err != nil && err != io.EOF {
		return 0, time.Time{}, err
	}
	lines := strings.Split(string(s.buf[:n]), "\n")
	if len(lines) < 2 {
		return 0, time.Time{}, fmt.Errorf("Incorrect format of incoming data: there must be at least 2 lines in %v", lines)
	}
	splitted := strings.Split(lines[1], ", ")
	temp := strings.Split(splitted[0], "uptime: ")
	if len(temp) < 2 {
		return 0, time.Time{}, fmt.Errorf("Incorrect format of incoming data: expected \"uptime: x\" format in %s", splitted[0])
	}
	uptime, err := ParseDuration(temp[1])
	if err != nil {
		return 0, time.Time{}, fmt.Errorf("Cannot parse uptime: %s", err)
	}
	temp = strings.Split(splitted[1], "since ")
	if len(temp) < 2 {
		return uptime, time.Time{}, fmt.Errorf("Incorrect format of incoming data: expected \"since x\" format in %s", splitted[1])
	}
	since, err := time.Parse("Jan 2 15:04:05 2006 -0700", temp[1]+time.Now().Format(" -0700"))
	if err != nil {
		return uptime, time.Time{}, fmt.Errorf("Cannot parse since: %s", err)
	}
	since = since.In(time.Local)
	return uptime, since, nil
	/*sum.Since = since
	splitted = strings.Split(string(s.buf[:n]), "Security Associations (")
	if len(splitted) < 2 {
		return nil, fmt.Errorf("Incorrect format of incoming data: missing string \"Security Associations\"")
	}
	splitted = strings.Split(splitted[1], "):")
	splitted = strings.Split(splitted[0], ", ")
	if len(splitted) < 2 {
		return nil, fmt.Errorf("Incorrect format of incoming data: must be \"Security Associations (x up, y connecting)\"")
	}
	total, err := strconv.Atoi(strings.Split(splitted[0], " up")[0])
	if err != nil {
		return nil, fmt.Errorf("Cannot parse IKE SAs total: %s", err)
	}
	sum.SATotal = total
	half, err := strconv.Atoi(strings.Split(splitted[1], " connecting")[0])
	if err != nil {
		return nil, fmt.Errorf("Cannot parse IKE SAs total: %s", err)
	}
	sum.SAHalfOpened = half
	return &sum, nil*/
}
