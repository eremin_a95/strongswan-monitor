package strongswan

import (
	"bytes"
	"crypto/x509"
	"fmt"
	"io"
	"os/exec"
	"time"
)

// Cert is a struct storing Certificate and CA-tree info
type Cert struct {
	Cert   *x509.Certificate
	Issued []*Cert
	Issuer *Cert
	CRL    *CRL
}

// CRL is a struct storing CRL data
type CRL struct {
	Issuer      *Cert
	UpdateThis  time.Time
	UpdateNext  time.Time
	Serial      uint64
	AuthKeyID   string
	Revokations []*Revokation
}

// Revokation is a struct storing info about revoked certificate
type Revokation struct {
	Serial  uint64
	Time    time.Time
	Reaseon RevokationReason
}

// RevokationReason is a type enumerating revokation reasons of certificates
type RevokationReason int

// RevokationReason enumeration
const (
	UnknownReason RevokationReason = iota
	CACompromisedReason
	KeyCompromisedReason
)

// CertsReader is interface for types which
// can read slice of Certs from io.Reader
type CertsReader interface {
	Read(io.Reader) ([]*Cert, error)
}

// CertsGetter is a struct to handle with Certs getting
type CertsGetter struct {
	getCerts func(CertsReader) ([]*Cert, error)
	reader   CertsReader
}

// GetCerts is function which returns *Cert slice or error
func (cg *CertsGetter) GetCerts() ([]*Cert, error) {
	return cg.getCerts(cg.reader)
}

// NewCertsGetter return instance of CertsGetter or error
// if it is not possible to init any CertsReader
func NewCertsGetter() (*CertsGetter, error) {
	getter := new(CertsGetter)
	var err error
	getter.reader, err = NewViciCertsReader()
	if err == nil {
		getter.getCerts = func(reader CertsReader) ([]*Cert, error) {
			return reader.Read(nil)
		}
		if _, err = getter.getCerts(getter.reader); err == nil {
			return getter, nil
		}
	}
	getter.reader = new(IpsecCertsReader)
	getter.getCerts = func(reader CertsReader) ([]*Cert, error) {
		out, err := exec.Command("ipsec", "statusall").Output()
		if err != nil {
			return nil, fmt.Errorf("Cannot exec \"ipsec statusall\" command: %s", err)
		}
		certs, err := reader.Read(bytes.NewReader(out))
		if err != nil {
			return nil, fmt.Errorf("Cannot read certs by IpsecCertsReader: %s", err)
		}
		return certs, nil
	}
	if _, err := getter.getCerts(getter.reader); err == nil {
		return getter, nil
	}
	return nil, fmt.Errorf("Cannot initialize any CertsReader")
}
