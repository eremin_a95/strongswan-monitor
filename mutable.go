package main

// Mutable is interface for types which
// can mutate in the background.
// Every time Mutable instance mutates
// it executes Mutate(int).
// To know when Mutable instance has mutated
// read from Mutable.Channel()
type Mutable interface {
	Mutate(int)
	MutateChan() chan int
}

// SimpleMutable is simply implementation of Mutable interface
type SimpleMutable struct {
	mutateChan chan int
}

// Mutate is an implementation for Mutable interface
func (sm *SimpleMutable) Mutate(v int) {
	sm.mutateChan <- v
}

// MutateChan is an implementation for Mutable interface
func (sm *SimpleMutable) MutateChan() chan int {
	return sm.mutateChan
}

// NewSimpleMutable returns new instance of SimpleMutable
func NewSimpleMutable() *SimpleMutable {
	return &SimpleMutable{mutateChan: make(chan int)}
}

// MutableActivator is interface for mutable types
// which mutate only when they are activated
type MutableActivator interface {
	Activator
	Mutable
}
