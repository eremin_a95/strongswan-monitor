package main

import (
	"bytes"
	"testing"
	"time"
)

const ConfExample1 = `tabs:
- stats
- conns
- sas
- pools
- certs
conns: 2000
pools: 4000
summary: 4000
certs: 8000
`

func TestNewConfig(t *testing.T) {
	reader := bytes.NewBufferString(ConfExample1)
	config, err := NewConfig(reader)
	if err != nil {
		t.Errorf("err expected %v, get %v", nil, err)
		t.FailNow()
	}
	if config.ConnsPeriod != time.Second*2 {
		t.Errorf("config.ConnsPeriod expected %v, get %v", time.Second*2, config.ConnsPeriod)
	}
	if config.PoolsPeriod != time.Second*4 {
		t.Errorf("config.PoolsPeriod expected %v, get %v", time.Second*4, config.PoolsPeriod)
	}
	if config.SummaryPeriod != time.Second*4 {
		t.Errorf("config.SummaryPeriod expected %v, get %v", time.Second*4, config.SummaryPeriod)
	}
	if config.CertsPeriod != time.Second*8 {
		t.Errorf("config.ConnsPeriod expected %v, get %v", time.Second*8, config.CertsPeriod)
	}
	if len(config.Tabs) != 5 {
		t.Errorf("err expected %v, get %v", nil, err)
		t.FailNow()
	}
}
