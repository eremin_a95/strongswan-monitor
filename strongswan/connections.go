package strongswan

import (
	"bytes"
	"fmt"
	"io"
	"os/exec"
	"strings"
	"time"
)

// AuthType is type to enumerate types of peers/hosts authentication
type AuthType uint

// Types of peers/hosts authentication
const (
	UnknownAuth AuthType = iota
	NoAuth
	AnyAuth
	EAPMSCHAPv2Auth
	PublicKeyAuth
	PreSharedKeyAuth
)

func (a AuthType) String() string {
	switch a {
	case NoAuth:
		return "No authetication"
	case AnyAuth:
		return "Any authentication"
	case PublicKeyAuth:
		return "Public key authetication"
	case PreSharedKeyAuth:
		return "Pre-Shared key authetication"
	case EAPMSCHAPv2Auth:
		return "EAP_MSCHAPV2 authentication"
	default:
		return "Unknown authentication"
	}
}

// ParseAuthType returns AuthType according to str
func ParseAuthType(str string) AuthType {
	str = strings.ToLower(str)
	switch str {
	case "any authentication", "any reauthentication":
		return AnyAuth
	case "public key", "public key authentication", "public key reauthentication":
		return PublicKeyAuth
	case "pre-shared key", "pre-shared key authentication", "pre-shared key reauthentication":
		return PreSharedKeyAuth
	case "no authentication":
		return NoAuth
	default:
		str = strings.Split(str, " with eap")[0]
		switch str {
		case "eap_mschapv2", "eap_mschapv2 authentication", "eap_mschapv2 reauthentication":
			return EAPMSCHAPv2Auth
		}
		return UnknownAuth
	}
}

// IKEVersion is type to enumerate version of IKE protocol
type IKEVersion uint

// There is two versions of IKE: IKEv1 and IKEv2
const (
	IKEUnknown IKEVersion = 0
	IKEv1      IKEVersion = 1
	IKEv2      IKEVersion = 1 << 1
)

func (i IKEVersion) String() string {
	switch i {
	case IKEv1 | IKEv2:
		return "IKEv1/v2"
	case IKEv1:
		return "IKEv1"
	case IKEv2:
		return "IKEv2"
	default:
		return "Unknown IKE version"
	}
}

// ParseIKEVersion parses IKEVersion from string
func ParseIKEVersion(str string) IKEVersion {
	switch strings.ToLower(str) {
	case "ikev1", "1", "ike1", "ike v1", "ike 1":
		return IKEv1
	case "ikev2", "2", "ike2", "ike v2", "ike 2":
		return IKEv2
	case "ikev1/v2", "ikev1/2", "ikev1|ikev2", "0", "1/2", "ike1/2", "ike v1/v2", "ike 1/2":
		return IKEv1 | IKEv2
	default:
		return IKEUnknown
	}
}

// ConnectionMode is type to enumerate connection modes
type ConnectionMode uint

// There is two connection modes: TRANSPORT and TUNNEL
const (
	UnknownMode ConnectionMode = iota
	TransportMode
	TunnelMode
	PassMode
)

func (cm ConnectionMode) String() string {
	switch cm {
	case TunnelMode:
		return "TUNNEL"
	case TransportMode:
		return "TRANSPORT"
	case PassMode:
		return "PASS"
	default:
		return "UnknownMode"
	}
}

// ParseConnectionMode returns ConnectionMode according to str
func ParseConnectionMode(str string) ConnectionMode {
	str = strings.ToUpper(str)
	switch str {
	case "TUNNEL":
		return TunnelMode
	case "TRANSPORT":
		return TransportMode
	case "PASS":
		return PassMode
	default:
		return UnknownMode
	}
}

// Connection is type for storing info about connection
type Connection struct {
	Name                  string
	LocalAddr, RemoteAddr []string
	LocalAuth, RemoteAuth []AuthType
	LocalID, RemoteID     []string
	Children              []Child
	Version               IKEVersion
	SAs                   []*SA
}

// Child is type for storing info about child association of connection
type Child struct {
	Mode             ConnectionMode
	Rekeying, Reauth time.Duration
	Local, Remote    []string
}

func (c Child) String() string {
	return fmt.Sprintf("%s==(%s)==%s", c.Local, c.Mode, c.Remote)
}

// ConnectionsReader is interface for types which
// can read slice of Connections from io.Reader
type ConnectionsReader interface {
	Read(io.Reader) ([]*Connection, error)
}

// ConnectionsGetter is a struct to handle with Connections getting
type ConnectionsGetter struct {
	getConns func(ConnectionsReader) ([]*Connection, error)
	reader   ConnectionsReader
}

// GetConns is function which returns *Connection slice or error
func (cg *ConnectionsGetter) GetConns() ([]*Connection, error) {
	return cg.getConns(cg.reader)
}

// NewConnectionsGetter return instance of ConnectionsGetter or error
// if it is not possible to init any ConnectionsReader
func NewConnectionsGetter() (*ConnectionsGetter, error) {
	getter := new(ConnectionsGetter)
	var err error
	getter.reader, err = NewViciConnReader()
	if err == nil {
		getter.getConns = func(reader ConnectionsReader) ([]*Connection, error) {
			return reader.Read(nil)
		}
		if _, err = getter.getConns(getter.reader); err == nil {
			return getter, nil
		}
	}
	getter.reader = new(IpsecConnReader)
	getter.getConns = func(reader ConnectionsReader) ([]*Connection, error) {
		out, err := exec.Command("ipsec", "statusall").Output()
		if err != nil {
			return nil, fmt.Errorf("Cannot exec \"ipsec statusall\" command: %s", err)
		}
		conns, err := reader.Read(bytes.NewReader(out))
		if err != nil {
			return nil, fmt.Errorf("Cannot read connections by IpsecConnReader: %s", err)
		}
		return conns, nil
	}
	if _, err = getter.getConns(getter.reader); err == nil {
		return getter, nil
	}
	return nil, fmt.Errorf("Cannot initialize any ConnectionsReader")
}
