package main

import (
	"log"
	"os"
	"time"

	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
	"gitlab.com/eremin_a95/strongswan-monitor/strongswan"
)

// UIEventHandler is interface for types which
// can handle incoming ui.Event
type UIEventHandler interface {
	HandleUIEvent(ui.Event)
}

// Tab is interface for types which
// is MutableActivator, UIEventHandler and Drawable
// and has period
type Tab interface {
	UIEventHandler
	MutableActivator
	ui.Drawable
}

// NewTab return instance of tab depending on the tabType and its name
func NewTab(tabType TabType) (Tab, string) {
	switch tabType {
	case StatsTabType:
		return NewStatsPanel(), "Statistics"
	case ConnsTabType:
		return NewConnectionsPanel(), "Connections"
	case SAsTabType:
		return NewSAsPanel(), "SAs"
	case PoolsTabType:
		return NewPoolsPanel(), "Pools"
	case CertsTabType:
		return NewCertsPanel(), "Certs"
	default:
		return nil, "Unknown"
	}
}

var info *strongswan.Info

func usingDefaultConfWarning() {
	log.Printf("WARNING!!! Using DEFAULT configuration in")
	time.Sleep(time.Second)
	log.Printf("3 seconds")
	time.Sleep(time.Second)
	log.Printf("2 seconds")
	time.Sleep(time.Second)
	log.Printf("1 seconds")
	time.Sleep(time.Second)
}

func readConfig(filename string) *Config {
	file, err := os.Open(filename)
	if err != nil {
		log.Printf("Error while opening configuration file %s: %s", filename, err)
		usingDefaultConfWarning()
		return &DefaultConfig
	}
	config, err := NewConfig(file)
	if err != nil {
		log.Printf("Error while reading configuration file %s: %s", filename, err)
		usingDefaultConfWarning()
		return &DefaultConfig
	}
	return config
}

// createTabs creates slice of Tab and new widgets.TabPane
// according to configuration c
func createTabs(c *Config) ([]Tab, *widgets.TabPane) {
	termX, termY := ui.TerminalDimensions()
	tabs := make([]Tab, len(c.Tabs))
	tp := widgets.NewTabPane()
	tp.SetRect(0, 0, termX, 3)
	if len(tabs) == 0 {
		return tabs, tp
	}
	var name string
	for i := range tabs {
		tabs[i], name = NewTab(c.Tabs[i])
		tp.TabNames = append(tp.TabNames, name)
		tabs[i].SetRect(0, 3, termX, termY)
	}
	tabs[0].Activate()
	return tabs, tp
}

func main() {
	var confFileName = "strongswan-monitor.yaml"
	if len(os.Args) == 3 && os.Args[1] == "-c" {
		confFileName = os.Args[2]
	}
	config := readConfig(confFileName)

	if len(config.Tabs) <= 0 {
		log.Fatalf("There is no tabs in config!")
	}

	var err error
	info, err = strongswan.NewInfo(config.SummaryPeriod, config.PoolsPeriod, config.ConnsPeriod, config.CertsPeriod)
	if err != nil {
		log.Fatalf("Failed to initialize strongswan.Info: %v", err)
	}

	if err := ui.Init(); err != nil {
		log.Fatalf("Failed to initialize termui: %v", err)
	}

	tabs, tabpane := createTabs(config)

	render := func() {
		ui.Clear()
		ui.Render(tabpane, tabs[tabpane.ActiveTabIndex])
	}
	render()

	go func() {
		for {
			select {
			case <-tabs[tabpane.ActiveTabIndex].MutateChan():
				render()
			default:
				time.Sleep(time.Millisecond * 100)
			}
		}
	}()

	activate := func() {
		for i := range tabs {
			tabs[i].Deactivate()
		}
		tabs[tabpane.ActiveTabIndex].Activate()
		tabs[tabpane.ActiveTabIndex].Mutate(0)
	}

	quit := make(chan int)
	go func() {
		uiEvents := ui.PollEvents()
		for {
			e := <-uiEvents
			switch e.ID {
			case "q", "<C-c>":
				quit <- 0
			case "<Tab>", "<Space>":
				tabpane.ActiveTabIndex = (tabpane.ActiveTabIndex + 1) % len(tabpane.TabNames)
				activate()
			case "<C-<Space>>", "<Backspace>":
				tabpane.ActiveTabIndex = (tabpane.ActiveTabIndex + len(tabpane.TabNames) - 1) % len(tabpane.TabNames)
				activate()
			case "1":
				tabpane.ActiveTabIndex = 0
				activate()
			case "2":
				if len(tabpane.TabNames) >= 2 {
					tabpane.ActiveTabIndex = 1
					activate()
				}
			case "3":
				if len(tabpane.TabNames) >= 3 {
					tabpane.ActiveTabIndex = 2
					activate()
				}
			case "4":
				if len(tabpane.TabNames) >= 4 {
					tabpane.ActiveTabIndex = 3
					activate()
				}
			case "5":
				if len(tabpane.TabNames) >= 5 {
					tabpane.ActiveTabIndex = 4
					activate()
				}
			case "6":
				if len(tabpane.TabNames) >= 6 {
					tabpane.ActiveTabIndex = 5
					activate()
				}
			case "7":
				if len(tabpane.TabNames) >= 7 {
					tabpane.ActiveTabIndex = 6
					activate()
				}
			case "8":
				if len(tabpane.TabNames) >= 8 {
					tabpane.ActiveTabIndex = 7
					activate()
				}
			case "9":
				if len(tabpane.TabNames) >= 9 {
					tabpane.ActiveTabIndex = 8
					activate()
				}
			case "0":
				if len(tabpane.TabNames) >= 10 {
					tabpane.ActiveTabIndex = 9
					activate()
				}
			default:
				tabs[tabpane.ActiveTabIndex].HandleUIEvent(e)
			}
			render()
		}
	}()

	exitCode := <-quit
	ui.Close()
	os.Exit(exitCode)
}
