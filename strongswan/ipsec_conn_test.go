package strongswan

import (
	"bytes"
	"testing"
)

const IpsecConnExample1 = `Status of IKE charon daemon (strongSwan 5.8.0, Linux 5.1.3, x86_64):
  uptime: 2 seconds, since May 20 09:44:52 2019
  malloc: sbrk 3133440, mmap 0, used 2003936, free 1129504
  worker threads: 11 of 16 idle, 5/0/0/0 working, job queue: 0/0/0/0, scheduled: 6
  loaded plugins: charon random nonce aes sha1 sha2 pem pkcs1 curve25519 gmp x509 curl revocation hmac stroke kernel-netlink socket-default updown
Virtual IP pools (size/online/offline):
  10.3.0.0/28: 14/2/0
Listening IP addresses:
  192.168.0.1
  fec0::1
  10.1.0.1
  fec1::1
Connections:
	  rw:  192.168.0.1...%any  IKEv2
	  rw:   local:  [moon.strongswan.org] uses public key authentication
	  rw:    cert:  \"C=CH, O=strongSwan Project, CN=moon.strongswan.org\"
	  rw:   remote: uses EAP_MSCHAPV2 authentication with EAP identity '%any'
	  rw:   child:  10.1.0.0/16 === dynamic TUNNEL
Security Associations (2 up, 0 connecting):
	  rw[3]: DELETING, 192.168.0.1[moon.strongswan.org]...192.168.0.100[carol@strongswan.org]
	  rw[3]: IKEv2 SPIs: 644731b1e0447ae6_i a14e1d4b4415397e_r*
	  rw[3]: IKE proposal: AES_CBC_128/HMAC_SHA2_256_128/PRF_HMAC_SHA2_256/CURVE_25519
	  rw{3}:  Tasks active: IKE_DELETE 
	  rw{3}:  INSTALLED, TUNNEL, reqid 3, ESP in UDP SPIs: c838a5fe_i c705d783_o
	  rw{3}:  AES_CBC_128/HMAC_SHA2_256_128, 84 bytes_i (1 pkt, 1s ago), 84 bytes_o (1 pkt, 1s ago), rekeying in 14 minutes
	  rw{3}:   10.1.0.0/16 === 10.3.0.1/32
	  rw[2]: ESTABLISHED 1 second ago, 192.168.0.1[moon.strongswan.org]...192.168.0.200[dave@strongswan.org]
	  rw[2]: IKEv2 SPIs: 43007f5e20587c64_i bd4253212c4703b5_r*, public key reauthentication in 54 minutes
	  rw[2]: IKE proposal: AES_CBC_128/HMAC_SHA2_256_128/PRF_HMAC_SHA2_256/CURVE_25519
	  rw{2}:  INSTALLED, TUNNEL, reqid 2, ESP SPIs: c7879e63_i cad59f8b_o
	  rw{2}:  AES_CBC_128/HMAC_SHA2_256_128, 84 bytes_i (1 pkt, 1s ago), 84 bytes_o (1 pkt, 1s ago), rekeying in 14 minutes
	  rw{2}:   10.1.0.0/16 === 10.3.0.2/32
	  rw[1]: ESTABLISHED 2 seconds ago, 192.168.0.1[moon.strongswan.org]...192.168.0.100[carol@strongswan.org]
	  rw[1]: IKEv2 SPIs: 644731b1e0447ae6_i a14e1d4b4415397e_r*, public key reauthentication in 55 minutes
	  rw[1]: IKE proposal: AES_CBC_128/HMAC_SHA2_256_128/PRF_HMAC_SHA2_256/CURVE_25519
	  rw{1}:  INSTALLED, TUNNEL, reqid 1, ESP in UDP SPIs: c838a5fe_i c705d783_o
	  rw{1}:  AES_CBC_128/HMAC_SHA2_256_128, 0 bytes_i, 0 bytes_o, rekeying in 14 minutes
	  rw{1}:   10.1.0.0/16 === 10.3.0.1/32`

const IpsecConnExample2 = `Status of IKE charon daemon (strongSwan 5.8.1, Linux 5.2.11, x86_64):
  uptime: 9 seconds, since Sep 02 10:38:28 2019
  malloc: sbrk 2977792, mmap 0, used 2008336, free 969456
  worker threads: 11 of 16 idle, 5/0/0/0 working, job queue: 0/0/0/0, scheduled: 9
  loaded plugins: charon random nonce aes sha1 sha2 pem pkcs1 curve25519 gmp x509 curl revocation hmac stroke kernel-netlink socket-default updown
Listening IP addresses:
  192.168.0.1
  fec0::1
  10.1.0.1
  fec1::1
Connections:
Security Associations (3 up, 0 connecting):
  none
`

const IpsecConnExample3 = `Status of IKE charon daemon (strongSwan 5.8.1, Linux 5.2.11, x86_64):
uptime: 9 seconds, since Sep 02 10:38:28 2019
malloc: sbrk 2977792, mmap 0, used 2008336, free 969456
worker threads: 11 of 16 idle, 5/0/0/0 working, job queue: 0/0/0/0, scheduled: 9
loaded plugins: charon random nonce aes sha1 sha2 pem pkcs1 curve25519 gmp x509 curl revocation hmac stroke kernel-netlink socket-default updown
Listening IP addresses:
192.168.0.1
fec0::1
10.1.0.1
fec1::1
Connections:
	  pass-ssh:  %any...%any  IKEv1/2
	  pass-ssh:   local:  uses public key authentication
	  pass-ssh:   remote: uses public key authentication
	  pass-ssh:   child:  0.0.0.0/0[tcp/ssh] === 0.0.0.0/0[tcp] PASS
	  trap-any:  %any...%any  IKEv1/2
	  trap-any:   local:  uses pre-shared key authentication
	  trap-any:   remote: uses pre-shared key authentication
	  trap-any:   child:  dynamic === dynamic TRANSPORT
Shunted Connections:
	  pass-ssh:  0.0.0.0/0[tcp/ssh] === 0.0.0.0/0[tcp] PASS
Routed Connections:
	  trap-any{1}:  ROUTED, TRANSPORT, reqid 1
	  trap-any{1}:   0.0.0.0/0 === 0.0.0.0/0
Security Associations (3 up, 0 connecting):
 	 trap-any[3]: ESTABLISHED 3 seconds ago, 192.168.0.1[192.168.0.1]...192.168.0.200[192.168.0.200]
	  trap-any[3]: IKEv2 SPIs: bc7a7b04b44468dd_i aac15aa590c10ece_r*, pre-shared key reauthentication in 56 minutes
	  trap-any[3]: IKE proposal: AES_CBC_128/HMAC_SHA2_256_128/PRF_HMAC_SHA2_256/CURVE_25519
	  trap-any{4}:  INSTALLED, TRANSPORT, reqid 2, ESP SPIs: cecbbae9_i c172f02e_o
	  trap-any{4}:  AES_CBC_128/HMAC_SHA2_256_128, 64 bytes_i (1 pkt, 3s ago), 64 bytes_o (1 pkt, 3s ago), rekeying in 15 minutes
	  trap-any{4}:   192.168.0.1/32 === 192.168.0.200/32
	  trap-any[2]: ESTABLISHED 6 seconds ago, 192.168.0.1[192.168.0.1]...192.168.0.100[192.168.0.100]
	  trap-any[2]: IKEv2 SPIs: 453b5af799ce347f_i* d3067438bab6b510_r, pre-shared key reauthentication in 51 minutes
	  trap-any[2]: IKE proposal: AES_CBC_128/HMAC_SHA2_256_128/PRF_HMAC_SHA2_256/CURVE_25519
	  trap-any{3}:  INSTALLED, TRANSPORT, reqid 1, ESP SPIs: c4d64d48_i c177ba03_o
	  trap-any{3}:  AES_CBC_128/HMAC_SHA2_256_128, 64 bytes_i (1 pkt, 6s ago), 64 bytes_o (1 pkt, 6s ago), rekeying in 14 minutes
	  trap-any{3}:   192.168.0.1/32 === 192.168.0.100/32
	  trap-any[1]: ESTABLISHED 8 seconds ago, 192.168.0.1[192.168.0.1]...192.168.0.2[192.168.0.2]
	  trap-any[1]: IKEv2 SPIs: f4de6390c9d3e57b_i* c77335f594239775_r, pre-shared key reauthentication in 52 minutes
	  trap-any[1]: IKE proposal: AES_CBC_128/HMAC_SHA2_256_128/PRF_HMAC_SHA2_256/CURVE_25519
	  trap-any{2}:  INSTALLED, TRANSPORT, reqid 1, ESP SPIs: ccf1290e_i cc21bcb0_o
	  trap-any{2}:  AES_CBC_128/HMAC_SHA2_256_128, 64 bytes_i (1 pkt, 7s ago), 64 bytes_o (1 pkt, 7s ago), rekeying in 15 minutes
	  trap-any{2}:   192.168.0.1/32 === 192.168.0.2/32`

const IpsecConnExample4 = `Status of IKE charon daemon (strongSwan 5.6.2, Linux 4.15.0-64-generic, x86_64):
uptime: 6 minutes, since Sep 23 15:44:01 2019
malloc: sbrk 2564096, mmap 0, used 767936, free 1796160
worker threads: 11 of 16 idle, 5/0/0/0 working, job queue: 0/0/0/0, scheduled: 4
loaded plugins: charon aesni aes rc2 sha2 sha1 md4 md5 mgf1 random nonce x509 revocation constraints pubkey pkcs1 pkcs7 pkcs8 pkcs12 pgp dnskey sshkey pem openssl fips-prf gmp agent xcbc hmac gcm attr kernel-netlink resolve socket-default connmark stroke vici updown eap-mschapv2 xauth-generic counters
Virtual IP pools (size/online/offline):
10.4.0.0/29: 6/0/2
Listening IP addresses:
192.168.0.121
10.10.10.10
Connections:
	  rw_1:  192.168.0.121...%any  IKEv2
	  rw_1:   local:  [192.168.0.121] uses public key authentication
	  rw_1:    cert:  "C=RU, O=CenterInform, CN=192.168.0.121"
	  rw_1:   remote: uses public key authentication
	  rw_1:   child:  10.10.10.0/24 === dynamic TUNNEL
	  rw_2:  192.168.0.121...%any  IKEv2
	  rw_2:   local:  [192.168.0.121] uses pre-shared key authentication
	  rw_2:   remote: uses pre-shared key authentication
	  rw_2:   child:  10.10.10.0/24 === dynamic TUNNEL
Security Associations (0 up, 0 connecting):
none
`

const IpsecConnExample5 = `Status of IKE charon daemon (strongSwan 5.6.2, Linux 4.15.0-64-generic, x86_64):
uptime: 27 minutes, since Sep 23 15:44:01 2019
malloc: sbrk 2564096, mmap 0, used 925952, free 1638144
worker threads: 11 of 16 idle, 5/0/0/0 working, job queue: 0/0/0/0, scheduled: 17
loaded plugins: charon aesni aes rc2 sha2 sha1 md4 md5 mgf1 random nonce x509 revocation constraints pubkey pkcs1 pkcs7 pkcs8 pkcs12 pgp dnskey sshkey pem openssl fips-prf gmp agent xcbc hmac gcm attr kernel-netlink resolve socket-default connmark stroke vici updown eap-mschapv2 xauth-generic counters
Virtual IP pools (size/online/offline):
10.4.0.0/29: 6/1/2
Listening IP addresses:
192.168.0.121
10.10.10.10
Connections:
	  rw_1:  192.168.0.121...%any  IKEv2
	  rw_1:   local:  [192.168.0.121] uses public key authentication
	  rw_1:    cert:  "C=RU, O=CenterInform, CN=192.168.0.121"
	  rw_1:   remote: uses public key authentication
	  rw_1:   child:  10.10.10.0/24 === dynamic TUNNEL
	  rw_2:  192.168.0.121...%any  IKEv2
	  rw_2:   local:  [192.168.0.121] uses pre-shared key authentication
	  rw_2:   remote: uses pre-shared key authentication
	  rw_2:   child:  10.10.10.0/24 === dynamic TUNNEL
Security Associations (1 up, 0 connecting):
	  rw_2[10]: DELETING, 192.168.0.121[192.168.0.121]...192.168.0.3[andrey]
	  rw_2[10]: IKEv2 SPIs: 1e0b74b8d35a75eb_i 70f8f61c144e8cc1_r*
	  rw_2[10]: IKE proposal: AES_CBC_128/HMAC_SHA2_256_128/PRF_HMAC_SHA2_256/ECP_256
	  rw_2[10]: Tasks active: IKE_DELETE 
	  rw_2{7}:  INSTALLED, TUNNEL, reqid 7, ESP in UDP SPIs: c532ac5a_i c39b57d9_o
	  rw_2{7}:  AES_CBC_128/HMAC_SHA2_256_128, 0 bytes_i, 168 bytes_o (2 pkts, 104s ago), rekeying in 12 minutes
	  rw_2{7}:   10.10.10.0/24 === 10.4.0.2/32
`

func TestIpsecConnReader(t *testing.T) {
	ipr := &IpsecConnReader{}

	reader := bytes.NewBufferString(IpsecConnExample1)
	conns, err := ipr.Read(reader)
	if err != nil {
		t.Error("For err expected", nil, "returned", err)
		t.FailNow()
	}
	if len(conns) != 1 {
		t.Error("For len(conns) expected", 1, "returned", len(conns))
		t.FailNow()
	}
	if conns[0].Name != "rw" {
		t.Error("For conns[0].Name expected rw returned", conns[0].Name)
	}
	if conns[0].LocalAddr[0] != "192.168.0.1" {
		t.Error("For conns[0].LocalAddr[0] expected 192.168.0.1 returned", conns[0].LocalAddr[0])
	}
	if conns[0].RemoteAddr[0] != "%any" {
		t.Error("For conns[0].RemoteAddr[0] expected %any returned", conns[0].RemoteAddr[0])
	}
	if conns[0].LocalID[0] != "moon.strongswan.org" {
		t.Error("For conns[0].LocalID[0] expected moon.strongswan.org returned", conns[0].LocalID[0])
	}
	if conns[0].LocalAuth[0] != PublicKeyAuth {
		t.Error("For conns[0].LocalAuth[0] expected", PublicKeyAuth, "returned", conns[0].LocalAuth[0])
	}
	if conns[0].RemoteAuth[0] != EAPMSCHAPv2Auth {
		t.Error("For conns[0].RemoteAuth[0] expected", EAPMSCHAPv2Auth, "returned", conns[0].RemoteAuth[0])
	}
	if conns[0].Version != IKEv2 {
		t.Error("For conns[0].Version expected", IKEv2, "returned", conns[0].Version)
	}
	if len(conns[0].Children) != 1 {
		t.Error("For len(conns[0].Children) expected", 1, "returned", len(conns[0].Children))
		t.FailNow()
	}
	if conns[0].Children[0].Mode != TunnelMode {
		t.Error("For conns[0].Children[0].Mode expected", TunnelMode, "returned", conns[0].Children[0].Mode)
	}
	if conns[0].Children[0].Local[0] != "10.1.0.0/16" {
		t.Error("For conns[0].Children[0].Local[0] expected 10.1.0.0/16 returned", conns[0].Children[0].Local[0])
	}
	if conns[0].Children[0].Remote[0] != "dynamic" {
		t.Error("For conns[0].Children[0].Remote[0] expected dynamic returned", conns[0].Children[0].Remote[0])
	}
	if len(conns[0].SAs) != 3 {
		t.Error("For len(conns[0].SAs) expected", 3, "returned", len(conns[0].SAs))
	}
	if !conns[0].SAs[1].IKESA.Established {
		t.Error("For conns[0].SAs[1].IKESA.Established expected", true, "returned", conns[0].SAs[1].IKESA.Established)
	}
	if conns[0].SAs[1].IKESA.Alive != "1 second" {
		t.Error("For conns[0].SAs[1].IKESA.Alive expected 1 second returned", conns[0].SAs[1].IKESA.Alive)
	}
	if conns[0].SAs[1].IKESA.LocalAddr != "192.168.0.1" {
		t.Error("For conns[0].SAs[1].IKESA.LocalAddr expected 192.168.0.1 returned", conns[0].SAs[1].IKESA.LocalAddr)
	}
	if conns[0].SAs[1].IKESA.LocalID != "moon.strongswan.org" {
		t.Error("For conns[0].SAs[1].IKESA.LocalID expected moon.strongswan.org returned", conns[0].SAs[1].IKESA.LocalID)
	}
	if conns[0].SAs[1].IKESA.RemoteAddr != "192.168.0.200" {
		t.Error("For conns[0].SAs[1].IKESA.RemoteAddr expected 192.168.0.200 returned", conns[0].SAs[1].IKESA.RemoteAddr)
	}
	if conns[0].SAs[1].IKESA.RemoteID != "dave@strongswan.org" {
		t.Error("For conns[0].SAs[1].IKESA.RemoteID expected dave@strongswan.org returned", conns[0].SAs[1].IKESA.RemoteID)
	}
	if conns[0].SAs[1].IKESA.Auth != PublicKeyAuth {
		t.Error("For conns[0].SAs[1].IKESA.Auth expected", PublicKeyAuth, "returned", conns[0].SAs[1].IKESA.Auth)
	}
	if conns[0].SAs[1].IKESA.Version != IKEv2 {
		t.Error("For conns[0].SAs[1].IKESA.Version expected", IKEv2, "returned", conns[0].SAs[1].IKESA.Version)
	}
	if conns[0].SAs[1].IKESA.InitiatorSPI != "43007f5e20587c64" {
		t.Error("For conns[0].SAs[1].IKESA.InitiatorSPI expected 43007f5e20587c64 returned", conns[0].SAs[1].IKESA.InitiatorSPI)
	}
	if conns[0].SAs[1].IKESA.ResponderSPI != "bd4253212c4703b5" {
		t.Error("For conns[0].SAs[1].IKESA.ResponderSPI expected bd4253212c4703b5 returned", conns[0].SAs[1].IKESA.ResponderSPI)
	}
	if conns[0].SAs[1].IKESA.UntilReauth != "54 minutes" {
		t.Error("For conns[0].SAs[1].IKESA.ResponderSPI expected 54 minutes returned", conns[0].SAs[1].IKESA.UntilReauth)
	}
	if conns[0].SAs[1].IKESA.Security != "AES_CBC_128/HMAC_SHA2_256_128/PRF_HMAC_SHA2_256/CURVE_25519" {
		t.Error("For conns[0].SAs[1].IKESA.Security expected AES_CBC_128/HMAC_SHA2_256_128/PRF_HMAC_SHA2_256/CURVE_25519 returned", conns[0].SAs[1].IKESA.Security)
	}
	if len(conns[0].SAs[1].ChildSAs) != 1 {
		t.Error("For len(conns[0].SAs[1].ChildSAs) expected", 1, "returned", len(conns[0].SAs[1].ChildSAs))
		t.FailNow()
	}
	if !conns[0].SAs[1].ChildSAs[0].Installed {
		t.Error("For conns[0].SAs[1].ChildSAs[0].Installed expected", true, "returned", conns[0].SAs[1].ChildSAs[0].Installed)
	}
	if conns[0].SAs[1].ChildSAs[0].Mode != TunnelMode {
		t.Error("For conns[0].SAs[1].ChildSAs[0].Mode expected", TunnelMode, "returned", conns[0].SAs[1].ChildSAs[0].Mode)
	}
	if conns[0].SAs[1].ChildSAs[0].ReqID != 2 {
		t.Error("For conns[0].SAs[1].ChildSAs[0].ReqID expected", 2, "returned", conns[0].SAs[1].ChildSAs[0].ReqID)
	}
	if conns[0].SAs[1].ChildSAs[0].Proto != ESPProto {
		t.Error("For conns[0].SAs[1].ChildSAs[0].Proto expected", ESPProto, "returned", conns[0].SAs[1].ChildSAs[0].Proto)
	}
	if conns[0].SAs[1].ChildSAs[0].UDPEncaps {
		t.Error("For conns[0].SAs[1].ChildSAs[0].UDPEncaps expected", false, "returned", conns[0].SAs[1].ChildSAs[0].UDPEncaps)
	}
	if conns[0].SAs[1].ChildSAs[0].InputSPI != "c7879e63" {
		t.Error("For conns[0].SAs[1].ChildSAs[0].InputSPI expected c7879e63 returned", conns[0].SAs[1].ChildSAs[0].InputSPI)
	}
	if conns[0].SAs[1].ChildSAs[0].OutputSPI != "cad59f8b" {
		t.Error("For conns[0].SAs[1].ChildSAs[0].OutputSPI expected cad59f8b returned", conns[0].SAs[1].ChildSAs[0].OutputSPI)
	}
	if conns[0].SAs[1].ChildSAs[0].Security != "AES_CBC_128/HMAC_SHA2_256_128" {
		t.Error("For conns[0].SAs[1].ChildSAs[0].OutputSPI expected AES_CBC_128/HMAC_SHA2_256_128 returned", conns[0].SAs[1].ChildSAs[0].Security)
	}
	if conns[0].SAs[1].ChildSAs[0].BytesIn != 84 {
		t.Error("For conns[0].SAs[1].ChildSAs[0].BytesIn expected", 84, "returned", conns[0].SAs[1].ChildSAs[0].BytesIn)
	}
	if conns[0].SAs[1].ChildSAs[0].PktsIn != 1 {
		t.Error("For conns[0].SAs[1].ChildSAs[0].PktsIn expected", 1, "returned", conns[0].SAs[1].ChildSAs[0].PktsIn)
	}
	if conns[0].SAs[1].ChildSAs[0].LastInAgo != "1s" {
		t.Error("For conns[0].SAs[1].ChildSAs[0].LastInAgo expected 1s returned", conns[0].SAs[1].ChildSAs[0].LastInAgo)
	}
	if conns[0].SAs[1].ChildSAs[0].BytesOut != 84 {
		t.Error("For conns[0].SAs[1].ChildSAs[0].BytesOut expected", 84, "returned", conns[0].SAs[1].ChildSAs[0].BytesOut)
	}
	if conns[0].SAs[1].ChildSAs[0].PktsOut != 1 {
		t.Error("For conns[0].SAs[1].ChildSAs[0].PktsOut expected", 1, "returned", conns[0].SAs[1].ChildSAs[0].PktsOut)
	}
	if conns[0].SAs[1].ChildSAs[0].LastOutAgo != "1s" {
		t.Error("For conns[0].SAs[1].ChildSAs[0].LastOutAgo expected 1s returned", conns[0].SAs[1].ChildSAs[0].LastOutAgo)
	}
	if conns[0].SAs[1].ChildSAs[0].UntilRekey != "14 minutes" {
		t.Error("For conns[0].SAs[1].ChildSAs[0].OutputSPI expected 1s returned", conns[0].SAs[1].ChildSAs[0].LastInAgo)
	}
	if conns[0].SAs[1].ChildSAs[0].LocalNet != "10.1.0.0/16" {
		t.Error("For conns[0].SAs[1].ChildSAs[0].LocalNet expected 10.1.0.0/16 returned", conns[0].SAs[1].ChildSAs[0].LocalNet)
	}
	if conns[0].SAs[1].ChildSAs[0].RemoteNet != "10.3.0.2/32" {
		t.Error("For conns[0].SAs[1].ChildSAs[0].RemoteNet expected 10.3.0.2/32 returned", conns[0].SAs[1].ChildSAs[0].RemoteNet)
	}
	if !conns[0].SAs[2].IKESA.Established {
		t.Error("For conns[0].SAs[2].IKESA.Established expected", true, "returned", conns[0].SAs[2].IKESA.Established)
	}
	if conns[0].SAs[2].IKESA.Alive != "2 seconds" {
		t.Error("For conns[0].SAs[2].IKESA.Alive expected 2 seconds returned", conns[0].SAs[2].IKESA.Alive)
	}
	if conns[0].SAs[2].IKESA.LocalAddr != "192.168.0.1" {
		t.Error("For conns[0].SAs[2].IKESA.LocalAddr expected 192.168.0.1 returned", conns[0].SAs[2].IKESA.LocalAddr)
	}
	if conns[0].SAs[2].IKESA.LocalID != "moon.strongswan.org" {
		t.Error("For conns[0].SAs[2].IKESA.LocalID expected moon.strongswan.org returned", conns[0].SAs[2].IKESA.LocalID)
	}
	if conns[0].SAs[2].IKESA.RemoteAddr != "192.168.0.100" {
		t.Error("For conns[0].SAs[2].IKESA.RemoteAddr expected 192.168.0.100 returned", conns[0].SAs[2].IKESA.RemoteAddr)
	}
	if conns[0].SAs[2].IKESA.RemoteID != "carol@strongswan.org" {
		t.Error("For conns[0].SAs[2].IKESA.RemoteID expected carol@strongswan.org returned", conns[0].SAs[2].IKESA.RemoteID)
	}
	if conns[0].SAs[2].IKESA.Auth != PublicKeyAuth {
		t.Error("For conns[0].SAs[2].IKESA.Auth expected", PublicKeyAuth, "returned", conns[0].SAs[2].IKESA.Auth)
	}
	if conns[0].SAs[2].IKESA.Version != IKEv2 {
		t.Error("For conns[0].SAs[2].IKESA.Version expected", IKEv2, "returned", conns[0].SAs[2].IKESA.Version)
	}
	if conns[0].SAs[2].IKESA.InitiatorSPI != "644731b1e0447ae6" {
		t.Error("For conns[0].SAs[2].IKESA.InitiatorSPI expected 644731b1e0447ae6 returned", conns[0].SAs[2].IKESA.InitiatorSPI)
	}
	if conns[0].SAs[2].IKESA.ResponderSPI != "a14e1d4b4415397e" {
		t.Error("For conns[0].SAs[2].IKESA.ResponderSPI expected a14e1d4b4415397e returned", conns[0].SAs[2].IKESA.ResponderSPI)
	}
	if conns[0].SAs[2].IKESA.UntilReauth != "55 minutes" {
		t.Error("For conns[0].SAs[2].IKESA.ResponderSPI expected 55 minutes returned", conns[0].SAs[2].IKESA.UntilReauth)
	}
	if conns[0].SAs[2].IKESA.Security != "AES_CBC_128/HMAC_SHA2_256_128/PRF_HMAC_SHA2_256/CURVE_25519" {
		t.Error("For conns[0].SAs[2].IKESA.Security expected AES_CBC_128/HMAC_SHA2_256_128/PRF_HMAC_SHA2_256/CURVE_25519 returned", conns[0].SAs[2].IKESA.Security)
	}
	if len(conns[0].SAs[2].ChildSAs) != 1 {
		t.Error("For len(conns[0].SAs[2].ChildSAs) expected", 1, "returned", len(conns[0].SAs[2].ChildSAs))
		t.FailNow()
	}
	if !conns[0].SAs[2].ChildSAs[0].Installed {
		t.Error("For conns[0].SAs[2].Installed expected", true, "returned", conns[0].SAs[2].ChildSAs[0].Installed)
	}
	if conns[0].SAs[2].ChildSAs[0].Mode != TunnelMode {
		t.Error("For conns[0].SAs[2].ChildSAs[0].Mode expected", TunnelMode, "returned", conns[0].SAs[2].ChildSAs[0].Mode)
	}
	if conns[0].SAs[2].ChildSAs[0].ReqID != 1 {
		t.Error("For conns[0].SAs[2].ChildSAs[0].ReqID expected", 1, "returned", conns[0].SAs[2].ChildSAs[0].ReqID)
	}
	if conns[0].SAs[2].ChildSAs[0].Proto != ESPProto {
		t.Error("For conns[0].SAs[2].ChildSAs[0].Proto expected", ESPProto, "returned", conns[0].SAs[2].ChildSAs[0].Proto)
	}
	if !conns[0].SAs[2].ChildSAs[0].UDPEncaps {
		t.Error("For conns[0].SAs[2].ChildSAs[0].UDPEncaps expected", true, "returned", conns[0].SAs[2].ChildSAs[0].UDPEncaps)
	}
	if conns[0].SAs[2].ChildSAs[0].InputSPI != "c838a5fe" {
		t.Error("For conns[0].SAs[2].ChildSAs[0].InputSPI expected c838a5fe returned", conns[0].SAs[2].ChildSAs[0].InputSPI)
	}
	if conns[0].SAs[2].ChildSAs[0].OutputSPI != "c705d783" {
		t.Error("For conns[0].SAs[2].ChildSAs[0].OutputSPI expected c705d783 returned", conns[0].SAs[2].ChildSAs[0].OutputSPI)
	}
	if conns[0].SAs[2].ChildSAs[0].Security != "AES_CBC_128/HMAC_SHA2_256_128" {
		t.Error("For conns[0].SAs[2].ChildSAs[0].OutputSPI expected AES_CBC_128/HMAC_SHA2_256_128 returned", conns[0].SAs[2].ChildSAs[0].Security)
	}
	if conns[0].SAs[2].ChildSAs[0].BytesIn != 0 {
		t.Error("For conns[0].SAs[2].ChildSAs[0].BytesIn expected", 0, "returned", conns[0].SAs[2].ChildSAs[0].BytesIn)
	}
	if conns[0].SAs[2].ChildSAs[0].PktsIn != 0 {
		t.Error("For conns[0].SAs[2].ChildSAs[0].PktsIn expected", 0, "returned", conns[0].SAs[2].ChildSAs[0].PktsIn)
	}
	if conns[0].SAs[2].ChildSAs[0].LastInAgo != "" {
		t.Error("For conns[0].SAs[2].ChildSAs[0].LastInAgo expected \"\" returned", conns[0].SAs[2].ChildSAs[0].LastInAgo)
	}
	if conns[0].SAs[2].ChildSAs[0].BytesOut != 0 {
		t.Error("For conns[0].SAs[2].ChildSAs[0].BytesOut expected", 0, "returned", conns[0].SAs[2].ChildSAs[0].BytesOut)
	}
	if conns[0].SAs[2].ChildSAs[0].PktsOut != 0 {
		t.Error("For conns[0].SAs[2].ChildSAs[0].PktsOut expected", 0, "returned", conns[0].SAs[2].ChildSAs[0].PktsOut)
	}
	if conns[0].SAs[2].ChildSAs[0].LastOutAgo != "" {
		t.Error("For conns[0].SAs[2].ChildSAs[0].LastOutAgo expected \"\" returned", conns[0].SAs[2].ChildSAs[0].LastOutAgo)
	}
	if conns[0].SAs[2].ChildSAs[0].UntilRekey != "14 minutes" {
		t.Error("For conns[0].SAs[2].ChildSAs[0].OutputSPI expected 1s returned", conns[0].SAs[2].ChildSAs[0].LastInAgo)
	}
	if conns[0].SAs[2].ChildSAs[0].LocalNet != "10.1.0.0/16" {
		t.Error("For conns[0].SAs[2].ChildSAs[0].LocalNet expected 10.1.0.0/16 returned", conns[0].SAs[2].ChildSAs[0].LocalNet)
	}
	if conns[0].SAs[2].ChildSAs[0].RemoteNet != "10.3.0.1/32" {
		t.Error("For conns[0].SAs[2].ChildSAs[0].RemoteNet expected 10.3.0.1/32 returned", conns[0].SAs[2].ChildSAs[0].RemoteNet)
	}

	reader = bytes.NewBufferString(IpsecConnExample2)
	conns, err = ipr.Read(reader)
	if err != nil {
		t.Error("For err expected", nil, "returned", err)
		t.FailNow()
	}
	if len(conns) != 0 {
		t.Error("For len(conns) expected", 1, "returned", len(conns))
	}

	reader = bytes.NewBufferString(IpsecConnExample3)
	conns, err = ipr.Read(reader)
	if err != nil {
		t.Error("For err expected", nil, "returned", err)
		t.FailNow()
	}
	if len(conns) != 2 {
		t.Error("For len(conns) expected", 2, "returned", len(conns))
		t.FailNow()
	}
	if conns[0].Name != "pass-ssh" {
		t.Error("For conns[0].Name expected pass-ssh returned", conns[0].Name)
	}
	if conns[0].LocalAddr[0] != "%any" {
		t.Error("For conns[0].LocalAddr[0] expected %any returned", conns[0].LocalAddr[0])
	}
	if conns[0].RemoteAddr[0] != "%any" {
		t.Error("For conns[0].RemoteAddr[0] expected %any returned", conns[0].RemoteAddr[0])
	}
	if len(conns[0].LocalID) != 0 {
		t.Error("For len(conns[0].LocalID) expected 0 returned", len(conns[0].LocalID))
	}
	if conns[0].LocalAuth[0] != PublicKeyAuth {
		t.Error("For conns[0].LocalAuth[0] expected", PublicKeyAuth, "returned", conns[0].LocalAuth[0])
	}
	if conns[0].RemoteAuth[0] != PublicKeyAuth {
		t.Error("For conns[0].RemoteAuth[0] expected", PublicKeyAuth, "returned", conns[0].RemoteAuth[0])
	}
	if conns[0].Version != (IKEv2 | IKEv1) {
		t.Error("For conns[0].Version expected", IKEv2|IKEv1, "returned", conns[0].Version)
	}
	if len(conns[0].Children) != 1 {
		t.Error("For len(conns[0].Children) expected", 1, "returned", len(conns[0].Children))
		t.FailNow()
	}
	if conns[0].Children[0].Mode != PassMode {
		t.Error("For conns[0].Children[0].Local expected ", PassMode, "returned", conns[0].Children[0].Mode)
	}
	if conns[0].Children[0].Local[0] != "0.0.0.0/0[tcp/ssh]" {
		t.Error("For conns[0].Children[0].Local[0] expected 0.0.0.0/0[tcp/ssh] returned", conns[0].Children[0].Local[0])
	}
	if conns[0].Children[0].Remote[0] != "0.0.0.0/0[tcp]" {
		t.Error("For conns[0].Children[0].Remote[0] expected 0.0.0.0/0[tcp] returned", conns[0].Children[0].Remote[0])
	}
	if conns[1].Name != "trap-any" {
		t.Error("For conns[1].Name expected trap-any returned", conns[1].Name)
	}
	if conns[1].LocalAddr[0] != "%any" {
		t.Error("For conns[1].LocalAddr[0] expected %any returned", conns[1].LocalAddr[0])
	}
	if conns[1].RemoteAddr[0] != "%any" {
		t.Error("For conns[1].RemoteAddr[0] expected %any returned", conns[1].RemoteAddr[0])
	}
	if len(conns[1].LocalID) != 0 {
		t.Error("For len(conns[1].LocalID) expected 0 returned", len(conns[1].LocalID))
	}
	if conns[1].LocalAuth[0] != PreSharedKeyAuth {
		t.Error("For conns[1].LocalAuth[0] expected", PreSharedKeyAuth, "returned", conns[1].LocalAuth[0])
	}
	if conns[1].RemoteAuth[0] != PreSharedKeyAuth {
		t.Error("For conns[1].RemoteAuth[0] expected", PreSharedKeyAuth, "returned", conns[1].RemoteAuth[0])
	}
	if conns[1].Version != (IKEv2 | IKEv1) {
		t.Error("For conns[1].Version expected", IKEv2|IKEv1, "returned", conns[1].Version)
	}
	if len(conns[1].Children) != 1 {
		t.Error("For len(conns[1].Children) expected", 1, "returned", len(conns[1].Children))
		t.FailNow()
	}
	if conns[1].Children[0].Mode != TransportMode {
		t.Error("For conns[1].Children[0].Local expected", TransportMode, "returned", conns[1].Children[0].Mode)
	}
	if conns[1].Children[0].Local[0] != "dynamic" {
		t.Error("For conns[1].Children[0].Local[0] expected dynamic returned", conns[1].Children[0].Local[0])
	}
	if conns[1].Children[0].Remote[0] != "dynamic" {
		t.Error("For conns[1].Children[0].Remote[0] expected dynamic returned", conns[1].Children[0].Remote[0])
	}
	if len(conns[1].SAs) != 3 {
		t.Error("For len(conns[1].SAs) expected", 3, "returned", len(conns[1].SAs))
		t.FailNow()
	}
	if conns[1].SAs[0].IKESA.IsInitiator != false {
		t.Error("For conns[1].SAs[0].IKESA.IsInitiator expected false returned", conns[1].SAs[0].IKESA.IsInitiator)
	}
	if conns[1].SAs[1].IKESA.IsInitiator != true {
		t.Error("For conns[1].SAs[1].IKESA.IsInitiator expected true returned", conns[1].SAs[1].IKESA.IsInitiator)
	}
	if conns[1].SAs[2].IKESA.IsInitiator != true {
		t.Error("For conns[1].SAs[2].IKESA.IsInitiator expected true returned", conns[1].SAs[2].IKESA.IsInitiator)
	}

	reader = bytes.NewBufferString(IpsecConnExample4)
	conns, err = ipr.Read(reader)
	if err != nil {
		t.Error("For err expected", nil, "returned", err)
		t.FailNow()
	}
	if len(conns) != 2 {
		t.Error("For len(conns) expected", 2, "returned", len(conns))
		t.FailNow()
	}
	if len(conns[0].SAs) != 0 {
		t.Error("For len(conns[0].SAs) expected", 0, "returned", len(conns[0].SAs))
	}
	if len(conns[1].SAs) != 0 {
		t.Error("For len(conns[1].SAs) expected", 0, "returned", len(conns[1].SAs))
	}

	reader = bytes.NewBufferString(IpsecConnExample5)
	conns, err = ipr.Read(reader)
	if err != nil {
		t.Error("For err expected", nil, "returned", err)
		t.FailNow()
	}
	if conns[1].SAs[0].ChildSAs[0].BytesIn != 0 {
		t.Error("For conns[1].SAs[0].ChildSAs[0].BytesIn expected", 0, "returned", conns[1].SAs[0].ChildSAs[0].BytesIn)
	}
	if conns[1].SAs[0].ChildSAs[0].BytesOut != 168 {
		t.Error("For conns[1].SAs[0].ChildSAs[0].BytesOut expected", 168, "returned", conns[1].SAs[0].ChildSAs[0].BytesOut)
	}
}
