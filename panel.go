package main

import (
	"image"

	ui "github.com/gizak/termui/v3"
)

// Panel is type which contains other Drawable objects
type Panel struct {
	ui.Block
	contents                []ui.Drawable
	Title                   string
	Border                  bool
	BorderStyle, TitleStyle ui.Style
}

// Draw is function to implement ui.Drawable interface.
// Draw Panel and all its contents
func (p *Panel) Draw(buf *ui.Buffer) {
	p.Block.Title = p.Title
	p.Block.TitleStyle = p.TitleStyle
	p.Block.Border = p.Border
	p.Block.BorderStyle = p.BorderStyle
	p.Block.Draw(buf)
	for _, v := range p.contents {
		v.Draw(buf)
	}
}

// GetRect is function to implement ui.Drawable interface.
// Returns Rectangle of Panel
func (p *Panel) GetRect() image.Rectangle {
	return p.Block.GetRect()
}

// SetRect is function to implement ui.Drawable interface.
// Sets Panel Rectangle and move all Panel's contents
func (p *Panel) SetRect(x1, y1, x2, y2 int) {
	oldRect := p.Block.GetRect()
	dx1 := x1 - oldRect.Min.X
	dy1 := y1 - oldRect.Min.Y
	// Move all contents
	for _, v := range p.contents {
		old := v.GetRect()
		newX1 := old.Min.X + dx1
		newY1 := old.Min.Y + dy1
		// If new rect goes beyond new panel borders
		// limit new rect to borders
		newX2 := old.Max.X + dx1
		if newX2 >= x2 {
			newX2 = x2 - 1
		}
		newY2 := old.Max.Y + dy1
		if newY2 >= y2 {
			newY2 = y2 - 1
		}
		v.SetRect(newX1, newY1, newX2, newY2)
	}
	p.Block.SetRect(x1, y1, x2, y2)
}

// Add adds Drawable object to Panel
// drawable's coordinates must be related to Panel's x1, y1
// (if drawable has (0, 0, 10, 10) it starts in Panel's x1 + 1, y1 +1)
// drawables must not be cahnged by SetRect after they added to Panel
func (p *Panel) Add(drawables ...ui.Drawable) {
	for _, drawable := range drawables {
		oldRect := p.Block.GetRect()
		x1 := oldRect.Min.X
		y1 := oldRect.Min.Y
		x2 := oldRect.Max.X
		y2 := oldRect.Max.Y
		old := drawable.GetRect()
		newX1 := old.Min.X + x1 + 1
		newY1 := old.Min.Y + y1 + 1
		// If new rect goes beyond new panel borders
		// limit new rect to borders
		newX2 := old.Max.X + x1 + 1
		if newX2 >= x2 {
			newX2 = x2 - 1
		}
		newY2 := old.Max.Y + y1 + 1
		if newY2 >= y2 {
			newY2 = y2 - 1
		}
		drawable.SetRect(newX1, newY1, newX2, newY2)
		p.contents = append(p.contents, drawable)
	}
}

// NewPanel creates new Panel with defined title
func NewPanel(title string) *Panel {
	panel := &Panel{
		Border:      true,
		BorderStyle: ui.NewStyle(ui.ColorWhite),
		Title:       title,
		TitleStyle:  ui.NewStyle(ui.ColorWhite),
		Block:       *ui.NewBlock(),
	}
	return panel
}
