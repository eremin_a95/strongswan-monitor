package strongswan

import (
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/asn1"
	"fmt"
	"io"
	"math/big"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// IpsecCertsReader is type to Read slice of Certs
// with `ipsec listall` format
type IpsecCertsReader struct {
	buf [8192]byte
}

func (icr *IpsecCertsReader) Read(r io.Reader) ([]*Cert, error) {
	n, err := r.Read(icr.buf[:])
	if err != nil && err != io.EOF {
		return nil, err
	}
	var endStr, caStr, crlStr string
	temp := string(icr.buf[:n])
	temp = strings.Split(temp, "List of registered IKE algorithms:")[0]
	splitted := strings.Split(temp, "List of X.509 CRLs\n\n")
	if len(splitted) == 2 {
		crlStr = splitted[1]
		//return nil, fmt.Errorf("Incorrect format of incoming data: missing line \"List of X.509 CRLs\"")
	}
	splitted = strings.Split(splitted[0], "List of X.509 CA Certificates\n\n")
	if len(splitted) == 2 {
		caStr = splitted[1]
		//return nil, fmt.Errorf("Incorrect format of incoming data: missing line \"List of X.509 CA Certificates\"")
	}
	splitted = strings.Split(splitted[0], "List of X.509 End Entity Certificates\n\n")
	if len(splitted) == 2 {
		endStr = splitted[1]
		//return nil, fmt.Errorf("Incorrect format of incoming data: missing line \"List of X.509 End Entity Certificates\"")
	}

	ca := make([]*Cert, 0, 4)
	if caStr != "" {
		ca, err = readCAs(caStr, ca)
		if err != nil {
			return nil, fmt.Errorf("Error parsing list of CA certificates: %s", err)
		}
	}
	if endStr != "" {
		ca, err = readEndCerts(endStr, ca)
		if err != nil {
			return nil, fmt.Errorf("Error parsing list of End Entity certificates: %s", err)
		}
	}
	if crlStr != "" {
		ca, err = readCRLs(crlStr, ca)
		if err != nil {
			return nil, fmt.Errorf("Error parsing list of CRLs: %s", err)
		}
	}

	return ca, nil
}

func parseDN(dn string) (pkix.Name, error) {
	name := pkix.Name{}
	splitted := strings.Split(dn, ",")
	for _, str := range splitted {
		keyVal := strings.Split(strings.TrimSpace(str), "=")
		if len(keyVal) != 2 {
			return name, fmt.Errorf("Cannot parse DN attribute \"%s\"", str)
		}
		switch keyVal[0] {
		case "C":
			name.Country = make([]string, 1)
			name.Country[0] = keyVal[1]
		case "O":
			name.Organization = make([]string, 1)
			name.Organization[0] = keyVal[1]
		case "OU":
			name.OrganizationalUnit = make([]string, 1)
			name.OrganizationalUnit[0] = keyVal[1]
		case "CN":
			name.CommonName = keyVal[1]
		case "L":
			name.Locality = make([]string, 1)
			name.Locality[0] = keyVal[1]
		case "STREET":
			name.StreetAddress = make([]string, 1)
			name.StreetAddress[0] = keyVal[1]
		case "ST", "SP", "S":
			name.Province = make([]string, 1)
			name.Province[0] = keyVal[1]
		case "PC":
			name.PostalCode = make([]string, 1)
			name.PostalCode[0] = keyVal[1]
		default:
			if name.ExtraNames == nil {
				name.ExtraNames = make([]pkix.AttributeTypeAndValue, 0, 2)
			}
			//name.ExtraNames = append(name.ExtraNames, pkix.AttributeTypeAndValue{Type: ???, Value: keyVal[1]})
			//TODO
		}
	}
	return name, nil
}

func parseCerts(lines []string) ([]*Cert, error) {
	certs := make([]*Cert, 0, 4)
	var cert *Cert
	for i := 0; i < len(lines); i++ {
		splitted := strings.Split(lines[i], ": ")
		if len(splitted) != 2 {
			return certs, fmt.Errorf("Incorrect format of incoming data: expected \": \" splitter in %s", lines[i])
		}
		switch strings.TrimSpace(splitted[0]) {
		case "subject":
			certs = append(certs, &Cert{Cert: new(x509.Certificate), Issued: make([]*Cert, 0, 4)})
			cert = certs[len(certs)-1]
			tmp := strings.TrimSpace(splitted[1])
			tmp = strings.TrimPrefix(tmp, "\"")
			tmp = strings.TrimSuffix(tmp, "\"")
			var err error
			cert.Cert.Subject, err = parseDN(tmp)
			if err != nil {
				return certs, fmt.Errorf("Error while parsing subject \"%s\": %s", tmp, err)
			}
		case "issuer":
			tmp := strings.TrimSpace(splitted[1])
			tmp = strings.TrimPrefix(tmp, "\"")
			tmp = strings.TrimSuffix(tmp, "\"")
			var err error
			cert.Cert.Issuer, err = parseDN(tmp)
			if err != nil {
				return certs, fmt.Errorf("Error while parsing issuer \"%s\": %s", tmp, err)
			}
		case "validity":
			tmp := strings.Split(strings.TrimSpace(splitted[1]), ", ok")[0]
			tm, err := time.ParseInLocation("not before Jan 02 15:04:05 2006", tmp, time.Local)
			if err != nil {
				return certs, fmt.Errorf("Cannot parse NotBefore time from \"%s\": %s", splitted[1], err)
			}
			cert.Cert.NotBefore = tm
			i++
			tmp = strings.Split(strings.TrimSpace(lines[i]), ", ok")[0]
			tm, err = time.ParseInLocation("not after  Jan 02 15:04:05 2006", tmp, time.Local)
			if err != nil {
				return certs, fmt.Errorf("Cannot parse NotAfter time from \"%s\": %s", tmp, err)
			}
			cert.Cert.NotAfter = tm
		case "serial":
			cert.Cert.SerialNumber = big.NewInt(0)
			_, ok := cert.Cert.SerialNumber.SetString(strings.ReplaceAll(strings.TrimSpace(splitted[1]), ":", ""), 16)
			if !ok {
				return certs, fmt.Errorf("Cannot parse SerialNumber from \"%s\"", splitted[1])
			}
		case "CRL URIs":
			uri, err := url.Parse(strings.TrimSpace(splitted[1]))
			if err != nil {
				return certs, fmt.Errorf("Cannot parse CRL URI from \"%s\": %s", splitted[1], err)
			}
			cert.Cert.URIs = make([]*url.URL, 0, 1)
			cert.Cert.URIs = append(cert.Cert.URIs, uri)
		case "authkeyId":
			id, ok := big.NewInt(0).SetString(strings.ReplaceAll(strings.TrimSpace(splitted[1]), ":", ""), 16)
			if !ok {
				return certs, fmt.Errorf("Cannot parse AuthKeyID from \"%s\"", splitted[1])
			}
			cert.Cert.AuthorityKeyId = id.Bytes()
		case "subjkeyId":
			id, ok := big.NewInt(0).SetString(strings.ReplaceAll(strings.TrimSpace(splitted[1]), ":", ""), 16)
			if !ok {
				return certs, fmt.Errorf("Cannot parse SubjKeyID from \"%s\"", splitted[1])
			}
			cert.Cert.SubjectKeyId = id.Bytes()
		case "pubkey":
			switch strings.SplitN(strings.TrimSpace(splitted[1]), " ", 1)[0] {
			case "RSA":
				cert.Cert.PublicKeyAlgorithm = x509.RSA
			case "ED25519":
				//TODO
			}
		case "pathlen":
			path, err := strconv.Atoi(strings.TrimSpace(splitted[1]))
			if err != nil {
				return certs, fmt.Errorf("Cannot parse MaxPathlLen from \"%s\": %s", splitted[1], err)
			}
			cert.Cert.MaxPathLen = path
		case "certificatePolicies":
			cert.Cert.PolicyIdentifiers = make([]asn1.ObjectIdentifier, 0, 4)
			for i++; strings.HasPrefix(strings.TrimSpace(lines[i]), "1.3."); i++ {
				//TODO
			}
		case "altNames":
			//TODO
		case "flags":
			//TODO
		case "keyid":
			//TODO
		case "subjkey":
			//TODO
		}
	}
	return certs, nil
}

func readCAs(str string, ca []*Cert) ([]*Cert, error) {
	lines := strings.Split(str, "\n  ")
	ca, err := parseCerts(lines)
	if err != nil {
		return nil, err
	}
	for i := range ca {
		ca[i].Cert.IsCA = true
	}
	return ca, nil
}

func readEndCerts(str string, ca []*Cert) ([]*Cert, error) {
	lines := strings.Split(str, "\n  ")
	endCerts, err := parseCerts(lines)
	if err != nil {
		return nil, fmt.Errorf("Error while parsing End Entity certificates: %s", err)
	}
	for i := range endCerts {
		dn := endCerts[i].Cert.Issuer.String()
		if dn == "" {
			continue
		}
		if dn == endCerts[i].Cert.Subject.String() {
			ca = append(ca, endCerts[i])
			continue
		}
		for j := range ca {
			if dn == ca[j].Cert.Subject.String() {
				endCerts[i].Issuer = ca[j]
				ca[j].Issued = append(ca[j].Issued, endCerts[i])
			}
		}
	}
	return ca, nil
}

func readCRLs(str string, ca []*Cert) ([]*Cert, error) {
	return ca, nil
}
