package main

import (
	"fmt"

	"gitlab.com/eremin_a95/strongswan-monitor/strongswan"

	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
)

// SAsPanel is type which represents Security Associations panel
type SAsPanel struct {
	Panel
	AtomicActivator
	Mutable
	connections *widgets.Tree
	nodes       []*widgets.TreeNode
}

// HandleUIEvent implements interface UIEventHandler
func (sp *SAsPanel) HandleUIEvent(e ui.Event) {
	switch e.ID {
	case "<Enter>":
		sp.connections.ToggleExpand()
	case "e":
		sp.connections.Expand()
	case "c":
		sp.connections.Collapse()
	case "E":
		sp.connections.ExpandAll()
	case "C":
		sp.connections.CollapseAll()
	case "<Down>":
		if len(sp.nodes) > 0 {
			sp.connections.ScrollDown()
		}
	case "<Up>":
		if len(sp.nodes) > 0 {
			sp.connections.ScrollUp()
		}
	default:
		return
	}
}

func fillNodeFromSA(node *widgets.TreeNode, sa *strongswan.SA) {
	if sa.IKESA.Established {
		node.Value = NodeString(fmt.Sprintf("%s (established %s ago)", sa.IKESA.RemoteID, sa.IKESA.Alive))
	} else if sa.IKESA.Deleting {
		node.Value = NodeString(fmt.Sprintf("%s (deleting)", sa.IKESA.RemoteID))
	}
	node.Nodes[0].Value = NodeString(fmt.Sprintf("%s <---> %s", sa.IKESA.LocalID, sa.IKESA.RemoteID))
	node.Nodes[0].Nodes[0].Value = NodeString(fmt.Sprintf("%s <---> %s", sa.IKESA.LocalAddr, sa.IKESA.RemoteAddr))
	node.Nodes[0].Nodes = node.Nodes[0].Nodes[0:1]
	for i := range sa.ChildSAs {
		newNode := new(widgets.TreeNode)
		newNode.Value = NodeString(fmt.Sprintf("%s <---> %s", sa.ChildSAs[i].LocalNet, sa.ChildSAs[i].RemoteNet))
		node.Nodes[0].Nodes = append(node.Nodes[0].Nodes, newNode)
	}
	node.Nodes[1].Value = NodeString(fmt.Sprintf("IKE SA (%s)", sa.IKESA.Version))
	irStr := "responder"
	if sa.IKESA.IsInitiator {
		irStr = "initiator"
	}
	node.Nodes[1].Nodes[0].Value = NodeString(fmt.Sprintf("SPIs: %s (initiator), %s (responder)", sa.IKESA.InitiatorSPI, sa.IKESA.ResponderSPI))
	node.Nodes[1].Nodes[1].Value = NodeString(fmt.Sprintf("This host is %s", irStr))
	node.Nodes[1].Nodes[2].Value = NodeString(fmt.Sprintf("%s, re-auth in %s", sa.IKESA.Auth, sa.IKESA.UntilReauth))
	node.Nodes[1].Nodes[3].Value = NodeString(fmt.Sprintf("Proposal: %s", sa.IKESA.Security))
	node.Nodes[2].Value = NodeString("Chald SAs")
	if len(node.Nodes[2].Nodes) > len(sa.ChildSAs) {
		node.Nodes[2].Nodes = node.Nodes[2].Nodes[:len(sa.ChildSAs)]
	}
	for i := len(node.Nodes[2].Nodes); i < len(sa.ChildSAs); i++ {
		node.Nodes[2].Nodes = append(node.Nodes[2].Nodes, &widgets.TreeNode{Nodes: make([]*widgets.TreeNode, 6)})
		for j := range node.Nodes[2].Nodes[i].Nodes {
			node.Nodes[2].Nodes[i].Nodes[j] = new(widgets.TreeNode)
		}
	}
	for i, node := range node.Nodes[2].Nodes {
		udp := ""
		if sa.ChildSAs[i].UDPEncaps {
			udp = " in UDP"
		}
		node.Value = NodeString(fmt.Sprintf("ReqID: %v", sa.ChildSAs[i].ReqID))
		node.Nodes[0].Value = NodeString(fmt.Sprintf("Phase2 (%s by %s%s)", sa.ChildSAs[i].Mode, sa.ChildSAs[i].Proto, udp))
		node.Nodes[1].Value = NodeString(fmt.Sprintf("SPIs: %s (In), %s (Out)", sa.ChildSAs[i].InputSPI, sa.ChildSAs[i].OutputSPI))
		node.Nodes[2].Value = NodeString(fmt.Sprintf("Suite: %s", sa.ChildSAs[i].Security))
		node.Nodes[3].Value = NodeString(fmt.Sprintf("Rekeying in %s", sa.ChildSAs[i].UntilRekey))
		inAgo := ""
		if sa.ChildSAs[i].BytesIn != 0 {
			inAgo = sa.ChildSAs[i].LastInAgo + " ago"
		}
		node.Nodes[4].Value = NodeString(fmt.Sprintf("In:  %s / %v packets %s", sa.ChildSAs[i].BytesIn, sa.ChildSAs[i].PktsIn, inAgo))
		outAgo := ""
		if sa.ChildSAs[i].BytesOut != 0 {
			outAgo = sa.ChildSAs[i].LastOutAgo + " ago"
		}
		node.Nodes[5].Value = NodeString(fmt.Sprintf("Out: %s / %v packets %s", sa.ChildSAs[i].BytesOut, sa.ChildSAs[i].PktsOut, outAgo))
	}
}

func initNodeForSA() *widgets.TreeNode {
	node := new(widgets.TreeNode)
	node.Nodes = make([]*widgets.TreeNode, 3)
	for k := range node.Nodes {
		node.Nodes[k] = new(widgets.TreeNode)
	}
	node.Nodes[0].Nodes = make([]*widgets.TreeNode, 2, 4)
	for k := range node.Nodes[0].Nodes {
		node.Nodes[0].Nodes[k] = new(widgets.TreeNode)
	}
	node.Nodes[1].Nodes = make([]*widgets.TreeNode, 4)
	for k := range node.Nodes[1].Nodes {
		node.Nodes[1].Nodes[k] = new(widgets.TreeNode)
	}
	node.Nodes[2].Nodes = make([]*widgets.TreeNode, 0, 4)
	//for k := range node.Nodes[2].Nodes {
	//	node.Nodes[2].Nodes[k] = new(widgets.TreeNode)
	//}
	return node
}

// Mutate reimplements Mutate method from Mutable interface
func (sp *SAsPanel) Mutate(v int) {
	if sp.IsActivated() {
		info.RLock()
		if len(sp.nodes) > len(info.Connections) {
			sp.nodes = sp.nodes[:len(info.Connections)]
			sp.connections.ScrollTop()
		}
		for i, v := range info.Connections {
			if len(sp.nodes) <= i {
				sp.nodes = append(sp.nodes, nil)
			}
			if sp.nodes[i] == nil {
				sp.nodes[i] = new(widgets.TreeNode)
				sp.nodes[i].Nodes = make([]*widgets.TreeNode, len(v.SAs))
			}
			if len(sp.nodes[i].Nodes) > len(v.SAs) {
				sp.nodes[i].Nodes = sp.nodes[i].Nodes[:len(v.SAs)]
				sp.connections.ScrollTop()
			}
			sp.nodes[i].Value = NodeString(v.Name)
			for j := range v.SAs {
				if len(sp.nodes[i].Nodes) <= j {
					sp.nodes[i].Nodes = append(sp.nodes[i].Nodes, nil)
				}
				if sp.nodes[i].Nodes[j] == nil {
					sp.nodes[i].Nodes[j] = initNodeForSA()
				}
				fillNodeFromSA(sp.nodes[i].Nodes[j], v.SAs[j])
			}
		}
		info.RUnlock()
		sp.connections.SetNodes(sp.nodes)
		sp.Mutable.Mutate(v)
	}
}

// NewSAsPanel creates new SAsPanel with list of connections
func NewSAsPanel() *SAsPanel {
	sp := &SAsPanel{
		Panel:       *NewPanel("Security Associations"),
		Mutable:     NewSimpleMutable(),
		connections: widgets.NewTree(),
		nodes:       make([]*widgets.TreeNode, 0, 4),
	}
	termX, termY := ui.TerminalDimensions()
	sp.SetRect(0, 0, termX, termY)
	sp.TitleStyle = ui.NewStyle(ui.ColorClear, ui.ColorClear, ui.ModifierBold)
	sp.BorderStyle = ui.NewStyle(ui.ColorClear, ui.ColorClear, ui.ModifierBold)
	sp.connections.SetRect(0, 0, termX, termY)
	sp.connections.Border = false
	sp.connections.SelectedRowStyle = ui.NewStyle(ui.ColorClear, ui.ColorClear, ui.ModifierBold)
	sp.Add(sp.connections)

	ch := info.ConnsRoutine.RegObserver()
	go func() {
		for <-ch == 0 {
			sp.Mutate(0)
		}
	}()

	return sp
}
