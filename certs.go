package main

import (
	"bytes"
	"crypto/sha1"
	"crypto/x509"

	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
	"gitlab.com/eremin_a95/strongswan-monitor/strongswan"
)

// CertsPanel is type which represents Certs panel
type CertsPanel struct {
	Panel
	AtomicActivator
	Mutable
	certs     *widgets.Tree
	nodes     []*widgets.TreeNode
	nodesHash []byte
}

// HandleUIEvent implements interface UIEventHandler
func (cp *CertsPanel) HandleUIEvent(e ui.Event) {
	switch e.ID {
	case "<Enter>":
		cp.certs.ToggleExpand()
	case "e":
		cp.certs.Expand()
	case "c":
		cp.certs.Collapse()
	case "E":
		cp.certs.ExpandAll()
	case "C":
		cp.certs.CollapseAll()
	case "<Down>":
		if len(cp.nodes) > 0 {
			cp.certs.ScrollDown()
		}
	case "<Up>":
		if len(cp.nodes) > 0 {
			cp.certs.ScrollUp()
		}
	default:
		return
	}
}

// Mutate reimplements Mutate method from Mutable interface
func (cp *CertsPanel) Mutate(v int) {
	if cp.IsActivated() {
		info.RLock()
		/*if len(cp.nodes) > len(info.Certs) {
			cp.nodes = cp.nodes[:len(info.Certs)]
			cp.certs.ScrollTop()
		}*/
		cp.nodes = certsToNodes(info.Certs)
		info.RUnlock()
		newHash := hashNodes(cp.nodes)
		if !bytes.Equal(cp.nodesHash, newHash) {
			cp.nodesHash = newHash
			cp.certs.SetNodes(cp.nodes)
			cp.Mutable.Mutate(v)
		}
	}
}

func certsToNodes(certs []*strongswan.Cert) []*widgets.TreeNode {
	nodes := make([]*widgets.TreeNode, len(certs))
	for i := range certs {
		nodes[i] = new(widgets.TreeNode)
		if certs[i].Cert.IsCA {
			nodes[i].Value = NodeString("CA: " + certs[i].Cert.Subject.CommonName)
		} else {
			nodes[i].Value = NodeString(certs[i].Cert.Subject.CommonName)
		}
		nodes[i].Nodes = make([]*widgets.TreeNode, 1, 3)
		nodes[i].Nodes[0] = certToNode(certs[i].Cert)
		if len(certs[i].Issued) > 0 {
			nodes[i].Nodes = append(nodes[i].Nodes, new(widgets.TreeNode))
			nodes[i].Nodes[1].Value = NodeString("Signed")
			nodes[i].Nodes[1].Nodes = certsToNodes(certs[i].Issued)
		}
		if certs[i].CRL != nil {
			nodes[i].Nodes = append(nodes[i].Nodes, new(widgets.TreeNode))
			nodes[i].Nodes[1].Value = NodeString("CRL")
		}
	}
	return nodes
}

func certToNode(cert *x509.Certificate) *widgets.TreeNode {
	node := new(widgets.TreeNode)
	node.Value = NodeString("Certificate")
	node.Nodes = make([]*widgets.TreeNode, 5, 6)
	for i := range node.Nodes {
		node.Nodes[i] = new(widgets.TreeNode)
	}
	node.Nodes[0].Value = NodeString("Serial Number: " + cert.SerialNumber.String())
	node.Nodes[1].Value = NodeString("Issuer: " + cert.Issuer.String())
	// TODO: fill issuer field
	node.Nodes[2].Value = NodeString("Subject: " + cert.Subject.String())
	// TODO: fill subject field
	node.Nodes[3].Value = NodeString("Valid after " + cert.NotBefore.String())
	node.Nodes[4].Value = NodeString("Valid until " + cert.NotAfter.String())
	return node
}

func xorSlices(a, b []byte) {
	for i := range a {
		a[i] ^= b[i]
	}
}

func hashNodes(nodes []*widgets.TreeNode) []byte {
	sum := make([]byte, sha1.Size)
	for i := range nodes {
		hash := sha1.Sum([]byte(nodes[i].Value.String()))
		xorSlices(sum, hash[:])
		xorSlices(sum, hashNodes(nodes[i].Nodes))
	}
	return sum
}

// NewCertsPanel creates new CertsPanel with list of connections
func NewCertsPanel() *CertsPanel {
	cp := &CertsPanel{
		Panel:   *NewPanel("Certs"),
		Mutable: NewSimpleMutable(),
		certs:   widgets.NewTree(),
		nodes:   make([]*widgets.TreeNode, 0, 4),
	}
	cp.nodesHash = hashNodes(cp.nodes)
	termX, termY := ui.TerminalDimensions()
	cp.SetRect(0, 0, termX, termY)
	cp.TitleStyle = ui.NewStyle(ui.ColorClear, ui.ColorClear, ui.ModifierBold)
	cp.BorderStyle = ui.NewStyle(ui.ColorClear, ui.ColorClear, ui.ModifierBold)
	cp.certs.SetRect(0, 0, termX, termY)
	cp.certs.Border = false
	cp.certs.SelectedRowStyle = ui.NewStyle(ui.ColorClear, ui.ColorClear, ui.ModifierBold)
	cp.Add(cp.certs)

	ch := info.CertsRoutine.RegObserver()

	go func() {
		for <-ch == 0 {
			cp.Mutate(0)
		}
	}()

	return cp
}
