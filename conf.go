package main

import (
	"fmt"
	"io"
	"time"

	"gopkg.in/yaml.v2"
)

// Config is struct represens yaml configuration struct
type Config struct {
	Tabs          []TabType     `yaml:"tabs"`
	ConnsPeriod   time.Duration `yaml:"conns"`
	PoolsPeriod   time.Duration `yaml:"pools"`
	SummaryPeriod time.Duration `yaml:"summary"`
	CertsPeriod   time.Duration `yaml:"certs"`
}

// TabType is enum for Tab types
type TabType string

// Types of Tab interface implementations
const (
	StatsTabType TabType = "stats"
	ConnsTabType TabType = "conns"
	SAsTabType   TabType = "sas"
	PoolsTabType TabType = "pools"
	CertsTabType TabType = "certs"
)

// DefaultConfig is default application configuration
var DefaultConfig = Config{
	[]TabType{
		StatsTabType,
		ConnsTabType,
		SAsTabType,
		PoolsTabType,
		CertsTabType,
	},
	time.Second * 2,
	time.Second * 4,
	time.Second * 4,
	time.Second * 8,
}

// NewConfig reads config from file and parses it to Config type
func NewConfig(reader io.Reader) (*Config, error) {
	config := &Config{
		Tabs:          make([]TabType, 0),
		SummaryPeriod: time.Hour,
		ConnsPeriod:   time.Hour,
		PoolsPeriod:   time.Hour,
		CertsPeriod:   time.Hour,
	}
	buf := make([]byte, 2048)
	n, err := reader.Read(buf)
	if err != nil {
		return nil, fmt.Errorf("Error while reading configuration file: %s", err)
	}
	err = yaml.Unmarshal(buf[:n], config)
	if err != nil {
		return nil, fmt.Errorf("Error while parsing configuration file to YAML format: %s", err)
	}
	config.SummaryPeriod *= time.Millisecond
	config.ConnsPeriod *= time.Millisecond
	config.PoolsPeriod *= time.Millisecond
	config.CertsPeriod *= time.Millisecond
	return config, nil
}
