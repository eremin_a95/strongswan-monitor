package main

import (
	"fmt"

	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
	"gitlab.com/eremin_a95/strongswan-monitor/strongswan"
)

// PoolsPanel is type which represents virtual IP-pools panel
type PoolsPanel struct {
	Panel
	AtomicActivator
	Mutable
	pools []strongswan.Pool
	list  *widgets.List
	sbc   *widgets.StackedBarChart
}

// HandleUIEvent implements interface UIEventHandler
func (pp *PoolsPanel) HandleUIEvent(e ui.Event) {
	switch e.ID {
	case "<Down>":
		if len(pp.list.Rows) > 0 {
			pp.list.ScrollDown()
			pp.updateChart()
		}
	case "<Up>":
		if len(pp.list.Rows) > 0 {
			pp.list.ScrollUp()
			pp.updateChart()
		}
	default:
		return
	}
}

func (pp *PoolsPanel) updateChart() {
	for i := range pp.sbc.LabelStyles {
		pp.sbc.LabelStyles[i].Modifier = ui.ModifierClear
	}
	pp.sbc.LabelStyles[pp.list.SelectedRow].Modifier = ui.ModifierBold
}

// Mutate reimplements Mutate method from Mutable interface
func (pp *PoolsPanel) Mutate(v int) {
	if pp.IsActivated() {
		info.RLock()
		pp.pools = append(pp.pools[0:0], info.Pools[:]...)
		info.RUnlock()
		if ln := len(pp.pools); ln < len(pp.list.Rows) {
			pp.list.Rows = pp.list.Rows[:ln]
			pp.sbc.Labels = pp.sbc.Labels[:ln]
			pp.sbc.Data = pp.sbc.Data[:ln]
		}
		for i, p := range pp.pools {
			if len(pp.list.Rows) <= i {
				pp.list.Rows = append(pp.list.Rows, p.String())
			} else {
				pp.list.Rows[i] = p.String()
			}
			if len(pp.sbc.Labels) <= i {
				pp.sbc.Labels = append(pp.sbc.Labels, p.Addr)
			} else {
				pp.sbc.Labels[i] = p.Addr
			}
			size := float64(p.Size)
			on := float64(p.On)
			off := float64(p.Off)
			free := (size - on - off) / size
			on = on / size
			off = off / size
			if len(pp.sbc.Data) <= i {
				pp.sbc.Data = append(pp.sbc.Data, []float64{free, on, off})
			} else {
				if pp.sbc.Data[i] == nil {
					pp.sbc.Data[i] = []float64{free, on, off}
				} else {
					pp.sbc.Data[i][0] = free
					pp.sbc.Data[i][1] = on
					pp.sbc.Data[i][2] = off
				}
			}
		}
		pp.updateChart()
		pp.Mutable.Mutate(0)
	}
}

// NewPoolsPanel creates new PoolsPanel with list of pools
func NewPoolsPanel() *PoolsPanel {
	pp := &PoolsPanel{
		Panel:   *NewPanel("Pools"),
		Mutable: NewSimpleMutable(),
		list:    widgets.NewList(),
		pools:   make([]strongswan.Pool, 0, 4),
		sbc:     widgets.NewStackedBarChart(),
	}
	termX, termY := ui.TerminalDimensions()
	pp.SetRect(0, 0, termX, termY)
	pp.TitleStyle = ui.NewStyle(ui.ColorClear, ui.ColorClear, ui.ModifierBold)
	pp.BorderStyle = ui.NewStyle(ui.ColorClear, ui.ColorClear, ui.ModifierBold)
	pp.list.SetRect(0, 0, termX, 6)
	pp.list.Border = false
	pp.list.SelectedRowStyle = ui.NewStyle(ui.ColorClear, ui.ColorClear, ui.ModifierBold)
	pp.sbc.SetRect(0, 6, termX, termY)
	pp.sbc.Border = false
	pp.sbc.Title = "Virtual IP pools: X-Axis=Pool, Y-Axis=% (Free, Online, Offline)"
	pp.sbc.BarWidth = 16
	pp.sbc.BarGap = 4
	pp.sbc.NumFormatter = func(v float64) string {
		return fmt.Sprintf("%.02f%%", v*100)
	}
	pp.sbc.BarColors = []ui.Color{ui.ColorBlue, ui.ColorGreen, ui.ColorYellow}
	/*pp.sbc.NumStyles = []ui.Style{
		ui.NewStyle(ui.ColorBlack, ui.Color(8)),
		ui.NewStyle(ui.ColorRed, ui.ColorGreen),
		ui.NewStyle(ui.ColorGreen, ui.ColorYellow),
	}*/
	pp.sbc.Data = make([][]float64, 0, 4)
	pp.sbc.Labels = make([]string, 0, 4)

	pp.Add(pp.list, pp.sbc)

	ch := info.PoolsRoutine.RegObserver()

	go func() {
		for <-ch == 0 {
			pp.Mutate(0)
		}
	}()

	return pp
}
