package strongswan

import (
	"fmt"
	"io"
	"strings"
	"time"
)

// SwanctlSummaryReader is type to Read Summary struct
// with `swanctl --stats` format
type SwanctlSummaryReader struct {
	buf [256]byte
}

func (s *SwanctlSummaryReader) Read(r io.Reader) (string, time.Time, error) {
	//sum := Summary{}
	n, err := r.Read(s.buf[:])
	if err != nil && err != io.EOF {
		return "", time.Time{}, err
	}
	lines := strings.Split(string(s.buf[:n]), "\n")
	splitted := strings.Split(lines[0], ", ")
	temp := strings.Split(splitted[0], "uptime: ")
	if len(temp) < 2 {
		return "", time.Time{}, fmt.Errorf("Incorrect format of incoming data: expected \"uptime: x\" format in %s", splitted[0])
	}
	uptime := temp[1]
	temp = strings.Split(splitted[1], "since ")
	if len(temp) < 2 {
		return uptime, time.Time{}, fmt.Errorf("Incorrect format of incoming data: expected \"since x\" format in %s", splitted[1])
	}
	since, err := time.Parse("Jan 2 15:04:05 2006", temp[1])
	if err != nil {
		return "", time.Time{}, fmt.Errorf("Cannot parse since: %s", err)
	}
	return uptime, since, nil
	/*sum.Since = since
	splitted = strings.Split(lines[4], "IKE_SAs: ")
	splitted = strings.Split(splitted[1], ", ")
	total, err := strconv.Atoi(strings.Split(splitted[0], " total")[0])
	if err != nil {
		return "", nil, fmt.Errorf("Cannot parse IKE SAs total: %s", err)
	}
	sum.SATotal = total
	half, err := strconv.Atoi(strings.Split(splitted[1], " half-open")[0])
	if err != nil {
		return "", nil, fmt.Errorf("Cannot parse IKE SAs total: %s", err)
	}
	sum.SAHalfOpened = half
	return "", &sum, nil*/
}
