package strongswan

import (
	"bytes"
	"fmt"
	"io"
	"os/exec"
)

// Pool is struct to store strongswan virtual pools info
type Pool struct {
	Addr, Name    string
	On, Off, Size int
}

func (p Pool) String() string {
	return fmt.Sprintf("%s\t\t%s: %v total, %v online, %v offline", p.Name, p.Addr, p.Size, p.On, p.Off)
}

// PoolsReader is interface for types which
// can read slice of Pools from io.Reader
type PoolsReader interface {
	Read(io.Reader) ([]Pool, error)
}

// PoolsGetter is a struct to handle with Pools getting
type PoolsGetter struct {
	getPools func(PoolsReader) ([]Pool, error)
	reader   PoolsReader
}

// GetPools is function which returns Pools slice or error
func (pg *PoolsGetter) GetPools() ([]Pool, error) {
	return pg.getPools(pg.reader)
}

// NewPoolsGetter return instance of PoolsGetter or error
// if it is not possible to init any PoolsReader
func NewPoolsGetter() (*PoolsGetter, error) {
	var reader PoolsReader
	var getPools func(PoolsReader) ([]Pool, error)
	reader = new(IpsecPoolsReader)
	getPools = func(reader PoolsReader) ([]Pool, error) {
		out, err := exec.Command("ipsec", "statusall").Output()
		if err != nil {
			return nil, fmt.Errorf("Cannot exec \"ipsec statusall\" command: %s", err)
		}
		pools, err := reader.Read(bytes.NewReader(out))
		if err != nil {
			return nil, fmt.Errorf("Cannot read pools by IpsecPoolsReader: %s", err)
		}
		return pools, nil
	}
	if _, err := getPools(reader); err == nil {
		getter := new(PoolsGetter)
		getter.reader = reader
		getter.getPools = getPools
		return getter, nil
	}
	return nil, fmt.Errorf("Cannot initialize any PoolsReader")
}
