package strongswan

import (
	"fmt"
	"strings"
)

// IPsecProto is type to enumerate IPsec Phase2 protocols
type IPsecProto uint

// There is two IPsec Phase2 protocols: AH and ESP
const (
	UnknownProto IPsecProto = iota
	AHProto
	ESPProto
)

func (p IPsecProto) String() string {
	switch p {
	case AHProto:
		return "AH"
	case ESPProto:
		return "ESP"
	default:
		return "Unknown"
	}
}

// ParseIPsecProto returns IPsecProto according to str
func ParseIPsecProto(str string) IPsecProto {
	str = strings.ToUpper(str)
	switch str {
	case "AH":
		return AHProto
	case "ESP":
		return ESPProto
	default:
		return UnknownProto
	}
}

// IKESA is struct to store info about IKE SA
type IKESA struct {
	Alive                      string
	LocalAddr, RemoteAddr      string
	LocalID, RemoteID          string
	InitiatorSPI, ResponderSPI string
	IsInitiator                bool
	Version                    IKEVersion
	Auth                       AuthType
	UntilReauth                string
	Security                   string
	Established                bool
	Deleting                   bool
}

// ChildSA is struct to store info about Child SA
type ChildSA struct {
	Mode                  ConnectionMode
	Proto                 IPsecProto
	UDPEncaps             bool
	ReqID                 int64
	InputSPI, OutputSPI   string
	LocalNet, RemoteNet   string
	IsLocalInitiator      bool
	Security              string
	BytesIn, BytesOut     ByteNum
	PktsIn, PktsOut       uint64
	LastInAgo, LastOutAgo string
	UntilRekey            string
	Installed             bool
}

// SA is struct to store info about IPsec Security Association
type SA struct {
	IKESA    IKESA
	ChildSAs []ChildSA
}

// ByteNum is Stringer type for handling byte numbers
type ByteNum uint64

// ByteNum units
const (
	ByteMultiplier ByteNum = 1024
	KiByte         ByteNum = ByteMultiplier
	MiByte         ByteNum = KiByte * ByteMultiplier
	GiByte         ByteNum = MiByte * ByteMultiplier
	TiByte         ByteNum = GiByte * ByteMultiplier
)

func (bn ByteNum) String() string {
	switch {
	case bn == 1:
		return "1 Byte"
	case bn < (KiByte * 4):
		return fmt.Sprintf("%v Bytes", uint64(bn))
	case bn < (MiByte * 4):
		return fmt.Sprintf("%0.2f KiBytes", float64(bn)/float64(KiByte))
	case bn < (GiByte * 2):
		return fmt.Sprintf("%0.2f MiBytes", float64(bn)/float64(MiByte))
	case bn < (TiByte * 2):
		return fmt.Sprintf("%0.2f GiBytes", float64(bn)/float64(GiByte))
	default:
		return fmt.Sprintf("%0.2f TiBytes", float64(bn)/float64(TiByte))
	}
}

// BitNum returns BitNum of this ByteNum
func (bn ByteNum) BitNum() BitNum {
	return BitNum(bn << 3)
}

// BitNum is Stringer type for handling bit numbers
type BitNum uint64

// BitNum units
const (
	BitMultiplier BitNum = 1000
	KBit          BitNum = BitMultiplier
	MBit          BitNum = KBit * BitMultiplier
	GBit          BitNum = MBit * BitMultiplier
	TBit          BitNum = GBit * BitMultiplier
)

func (bn BitNum) String() string {
	switch {
	case bn == 1:
		return "1 Bit"
	case bn < (KBit * 4):
		return fmt.Sprintf("%v Bits", uint64(bn))
	case bn < (MBit * 4):
		return fmt.Sprintf("%0.2f KBits", float64(bn)/float64(KBit))
	case bn < (GBit * 2):
		return fmt.Sprintf("%0.2f MBits", float64(bn)/float64(MBit))
	case bn < (TBit * 2):
		return fmt.Sprintf("%0.2f GBits", float64(bn)/float64(GBit))
	default:
		return fmt.Sprintf("%0.2f TBits", float64(bn)/float64(TBit))
	}
}

// ByteNum returns ByteNum of this BitNum
func (bn BitNum) ByteNum() ByteNum {
	return ByteNum(bn >> 3)
}
