package main

import "sync/atomic"

// Activator is interface for types which
// can be activated and deactivated.
// Instance of Activator can be activated multiple times,
// but first Deactivate() deactivates instance immediately
type Activator interface {
	Activate()
	Deactivate()
	IsActivated() bool
}

// AtomicActivator is type which activates/deactivates
// with atomic operations
type AtomicActivator struct {
	active uint32
}

// Activate is function to implement Activator interface.
// It activates instance with atomic Uint32 operations.
func (a *AtomicActivator) Activate() {
	atomic.StoreUint32(&a.active, 1)
}

// Deactivate is function to implement Activator interface.
// It deactivates instance with atomic Uint32 operations.
func (a *AtomicActivator) Deactivate() {
	atomic.StoreUint32(&a.active, 0)
}

// IsActivated is function to implement Activator interface.
// It checks if instance was activated with atomic Uint32 operations.
func (a *AtomicActivator) IsActivated() bool {
	return atomic.LoadUint32(&a.active) > 0
}
