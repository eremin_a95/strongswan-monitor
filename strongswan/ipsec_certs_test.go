package strongswan

import (
	"bytes"
	"testing"
)

const IpsecCertsExample1 = `
List of X.509 End Entity Certificates

  subject:  "C=CH, O=strongSwan Project, OU=Research, CN=carol@strongswan.org"
  issuer:   "C=CH, O=strongSwan Project, OU=Research, CN=Research CA"
  validity:  not before Aug 25 13:48:23 2019, ok
             not after  Aug 25 13:48:23 2027, ok (expires in 2914 days)
  serial:    01
  altNames:  carol@strongswan.org
  flags:     
  CRL URIs:  http://crl.strongswan.org/research.crl
  authkeyId: 9d:35:64:c7:76:29:29:ce:8c:89:92:f5:c4:82:7f:81:53:9e:25:b2
  subjkeyId: 0d:a4:15:26:6a:80:02:69:5e:a5:8c:bf:80:37:d4:86:4a:59:03:1a
  pubkey:    RSA 3072 bits
  keyid:     07:58:23:b3:f5:a5:35:a8:74:04:31:2d:e7:96:07:d5:f7:ae:c1:8c
  subjkey:   0d:a4:15:26:6a:80:02:69:5e:a5:8c:bf:80:37:d4:86:4a:59:03:1a

  subject:  "C=CH, O=strongSwan Project, CN=moon.strongswan.org"
  issuer:   "C=CH, O=strongSwan Project, CN=strongSwan Root CA"
  validity:  not before Aug 25 13:48:23 2019, ok
             not after  Aug 25 13:48:23 2027, ok (expires in 2914 days)
  serial:    03
  altNames:  moon.strongswan.org
  flags:     
  CRL URIs:  http://crl.strongswan.org/strongswan.crl
  authkeyId: cb:cb:df:7e:fc:d2:cb:d4:3d:ee:79:cd:72:0c:18:05:4b:18:c8:7e
  subjkeyId: d1:05:72:0b:9b:d3:09:63:6f:ac:7d:1a:c7:d5:42:73:55:69:6f:61
  pubkey:    RSA 3072 bits, has private key
  keyid:     4f:d6:db:04:31:9a:1c:ec:a7:03:94:f9:a2:06:bc:43:09:7f:42:98
  subjkey:   d1:05:72:0b:9b:d3:09:63:6f:ac:7d:1a:c7:d5:42:73:55:69:6f:61

  subject:  "C=CH, O=strongSwan Project, OU=Sales, CN=dave@strongswan.org"
  issuer:   "C=CH, O=strongSwan Project, OU=Sales, CN=Sales CA"
  validity:  not before Aug 25 13:48:23 2019, ok
             not after  Aug 25 13:48:23 2027, ok (expires in 2914 days)
  serial:    01
  altNames:  dave@strongswan.org
  flags:     
  CRL URIs:  http://crl.strongswan.org/sales.crl
  authkeyId: 38:97:11:76:1c:b0:e5:88:be:5a:f6:03:46:ae:fb:f9:20:e3:b5:27
  subjkeyId: 6f:6d:a8:08:23:43:30:b8:27:c5:96:ae:7a:74:d7:3a:01:47:05:34
  pubkey:    RSA 3072 bits
  keyid:     71:0f:4d:ea:14:e8:9a:46:a5:c3:a1:b4:63:c9:14:49:c0:e8:4b:80
  subjkey:   6f:6d:a8:08:23:43:30:b8:27:c5:96:ae:7a:74:d7:3a:01:47:05:34

List of X.509 CA Certificates

  subject:  "C=CH, O=strongSwan Project, OU=Research, CN=Research CA"
  issuer:   "C=CH, O=strongSwan Project, CN=strongSwan Root CA"
  validity:  not before Aug 25 13:48:23 2019, ok
             not after  Aug 25 13:48:23 2028, ok (expires in 3280 days)
  serial:    0b
  flags:     CA CRLSign 
  CRL URIs:  http://crl.strongswan.org/strongswan.crl
  authkeyId: cb:cb:df:7e:fc:d2:cb:d4:3d:ee:79:cd:72:0c:18:05:4b:18:c8:7e
  subjkeyId: 9d:35:64:c7:76:29:29:ce:8c:89:92:f5:c4:82:7f:81:53:9e:25:b2
  pubkey:    RSA 3072 bits
  keyid:     e8:8e:f9:45:51:18:d8:da:c8:4b:f3:6f:14:2c:62:dc:db:f9:87:e6
  subjkey:   9d:35:64:c7:76:29:29:ce:8c:89:92:f5:c4:82:7f:81:53:9e:25:b2

  subject:  "C=CH, O=strongSwan Project, CN=strongSwan Root CA"
  issuer:   "C=CH, O=strongSwan Project, CN=strongSwan Root CA"
  validity:  not before Aug 25 13:48:23 2019, ok
             not after  Aug 25 13:48:23 2029, ok (expires in 3645 days)
  serial:    50:b1:49:82:67:f0:72:da
  flags:     CA CRLSign self-signed 
  pathlen:   1
  subjkeyId: cb:cb:df:7e:fc:d2:cb:d4:3d:ee:79:cd:72:0c:18:05:4b:18:c8:7e
  pubkey:    RSA 3072 bits
  keyid:     eb:31:5c:68:4f:a9:3c:ba:1a:e3:9e:b1:d6:f9:b7:37:d7:90:6d:0b
  subjkey:   cb:cb:df:7e:fc:d2:cb:d4:3d:ee:79:cd:72:0c:18:05:4b:18:c8:7e

  subject:  "C=CH, O=strongSwan Project, OU=Sales, CN=Sales CA"
  issuer:   "C=CH, O=strongSwan Project, CN=strongSwan Root CA"
  validity:  not before Aug 25 13:48:23 2019, ok
             not after  Aug 25 13:48:23 2028, ok (expires in 3280 days)
  serial:    0c
  flags:     CA CRLSign 
  CRL URIs:  http://crl.strongswan.org/strongswan.crl
  authkeyId: cb:cb:df:7e:fc:d2:cb:d4:3d:ee:79:cd:72:0c:18:05:4b:18:c8:7e
  subjkeyId: 38:97:11:76:1c:b0:e5:88:be:5a:f6:03:46:ae:fb:f9:20:e3:b5:27
  pubkey:    RSA 3072 bits
  keyid:     53:f5:81:51:38:60:aa:45:be:8b:a1:88:13:d7:58:39:1d:f4:ba:e7
  subjkey:   38:97:11:76:1c:b0:e5:88:be:5a:f6:03:46:ae:fb:f9:20:e3:b5:27

List of X.509 CRLs

  issuer:   "C=CH, O=strongSwan Project, OU=Research, CN=Research CA"
  update:    this on Sep 02 10:09:16 2019, ok
             next on Sep 17 10:09:16 2019, ok (expires in 14 days)
  serial:    01
  authKeyId: 9d:35:64:c7:76:29:29:ce:8c:89:92:f5:c4:82:7f:81:53:9e:25:b2
  0 revoked certificates

  issuer:   "C=CH, O=strongSwan Project, CN=strongSwan Root CA"
  update:    this on Sep 02 10:09:16 2019, ok
             next on Sep 17 10:09:16 2019, ok (expires in 14 days)
  serial:    03
  authKeyId: cb:cb:df:7e:fc:d2:cb:d4:3d:ee:79:cd:72:0c:18:05:4b:18:c8:7e
  2 revoked certificates:
    0a: Aug 27 13:48:27 2019, ca compromise
    08: Aug 27 13:48:26 2019, key compromise

  issuer:   "C=CH, O=strongSwan Project, OU=Sales, CN=Sales CA"
  update:    this on Sep 02 10:09:16 2019, ok
             next on Sep 17 10:09:16 2019, ok (expires in 14 days)
  serial:    01
  authKeyId: 38:97:11:76:1c:b0:e5:88:be:5a:f6:03:46:ae:fb:f9:20:e3:b5:27
  0 revoked certificates

List of registered IKE algorithms:

  encryption: AES_CBC[aes]
  integrity:  HMAC_SHA1_96[hmac] HMAC_SHA1_128[hmac] HMAC_SHA1_160[hmac] HMAC_SHA2_256_128[hmac] HMAC_SHA2_256_256[hmac]
              HMAC_SHA2_384_192[hmac] HMAC_SHA2_384_384[hmac] HMAC_SHA2_512_256[hmac] HMAC_SHA2_512_512[hmac]
  aead:      
  hasher:     HASH_SHA1[sha1] HASH_SHA2_224[sha2] HASH_SHA2_256[sha2] HASH_SHA2_384[sha2] HASH_SHA2_512[sha2]
              HASH_IDENTITY[curve25519]
  prf:        PRF_KEYED_SHA1[sha1] PRF_HMAC_SHA1[hmac] PRF_HMAC_SHA2_256[hmac] PRF_HMAC_SHA2_384[hmac]
              PRF_HMAC_SHA2_512[hmac]
  xof:       
  dh-group:   CURVE_25519[curve25519] MODP_3072[gmp] MODP_4096[gmp] MODP_6144[gmp] MODP_8192[gmp] MODP_2048[gmp]
              MODP_2048_224[gmp] MODP_2048_256[gmp] MODP_1536[gmp] MODP_1024[gmp] MODP_1024_160[gmp] MODP_768[gmp]
              MODP_CUSTOM[gmp]
  random-gen: RNG_STRONG[random] RNG_TRUE[random]
  nonce-gen:  [nonce]

List of loaded Plugins:

charon:
    CUSTOM:libcharon
        NONCE_GEN
        CUSTOM:libcharon-sa-managers
        CUSTOM:libcharon-receiver
        CUSTOM:kernel-ipsec
        CUSTOM:kernel-net
    CUSTOM:libcharon-receiver
        HASHER:HASH_SHA1
        RNG:RNG_STRONG
        CUSTOM:socket
    CUSTOM:libcharon-sa-managers
        HASHER:HASH_SHA1
        RNG:RNG_WEAK
random:
    RNG:RNG_STRONG
    RNG:RNG_TRUE
nonce:
    NONCE_GEN
        RNG:RNG_WEAK
aes:
    CRYPTER:AES_CBC-16
    CRYPTER:AES_CBC-24
    CRYPTER:AES_CBC-32
sha1:
    HASHER:HASH_SHA1
    PRF:PRF_KEYED_SHA1
sha2:
    HASHER:HASH_SHA2_224
    HASHER:HASH_SHA2_256
    HASHER:HASH_SHA2_384
    HASHER:HASH_SHA2_512
pem:
    PRIVKEY:ANY
        PRIVKEY:ANY
        HASHER:HASH_MD5 (soft)
    PRIVKEY:RSA
        PRIVKEY:RSA
        HASHER:HASH_MD5 (soft)
    PRIVKEY:ECDSA (not loaded)
        PRIVKEY:ECDSA
        HASHER:HASH_MD5 (soft)
    PRIVKEY:DSA (not loaded)
        PRIVKEY:DSA
        HASHER:HASH_MD5 (soft)
    PRIVKEY:BLISS (not loaded)
        PRIVKEY:BLISS
    PRIVKEY:ED25519
        PRIVKEY:ED25519
    PUBKEY:ANY
        PUBKEY:ANY
    PUBKEY:RSA
        PUBKEY:RSA
    PUBKEY:ECDSA (not loaded)
        PUBKEY:ECDSA
    PUBKEY:DSA (not loaded)
        PUBKEY:DSA
    PUBKEY:BLISS (not loaded)
        PUBKEY:BLISS
    PUBKEY:ED25519
        PUBKEY:ED25519
    CERT_DECODE:ANY
        CERT_DECODE:X509 (soft)
        CERT_DECODE:PGP (soft)
    CERT_DECODE:X509
        CERT_DECODE:X509
    CERT_DECODE:X509_CRL
        CERT_DECODE:X509_CRL
    CERT_DECODE:OCSP_REQUEST (not loaded)
        CERT_DECODE:OCSP_REQUEST
    CERT_DECODE:OCSP_RESPONSE
        CERT_DECODE:OCSP_RESPONSE
    CERT_DECODE:X509_AC
        CERT_DECODE:X509_AC
    CERT_DECODE:PKCS10_REQUEST
        CERT_DECODE:PKCS10_REQUEST
    CERT_DECODE:PUBKEY (not loaded)
        CERT_DECODE:PUBKEY
    CERT_DECODE:PGP (not loaded)
        CERT_DECODE:PGP
    CONTAINER_DECODE:PKCS12 (not loaded)
        CONTAINER_DECODE:PKCS12
pkcs1:
    PRIVKEY:ANY
        PRIVKEY:RSA (soft)
        PRIVKEY:ECDSA (soft)
    PRIVKEY:RSA
    PUBKEY:ANY
        PUBKEY:RSA (soft)
        PUBKEY:ECDSA (soft)
        PUBKEY:ED25519 (soft)
        PUBKEY:ED448 (soft)
        PUBKEY:BLISS (soft)
        PUBKEY:DSA (soft)
    PUBKEY:RSA
curve25519:
    DH:CURVE_25519
        RNG:RNG_STRONG
    PRIVKEY:ED25519
    PRIVKEY_GEN:ED25519
        RNG:RNG_TRUE
        HASHER:HASH_SHA2_512
    PUBKEY:ED25519
    PRIVKEY_SIGN:ED25519
        HASHER:HASH_SHA2_512
    PUBKEY_VERIFY:ED25519
        HASHER:HASH_SHA2_512
    HASHER:HASH_IDENTITY
gmp:
    DH:MODP_3072
        RNG:RNG_STRONG
    DH:MODP_4096
        RNG:RNG_STRONG
    DH:MODP_6144
        RNG:RNG_STRONG
    DH:MODP_8192
        RNG:RNG_STRONG
    DH:MODP_2048
        RNG:RNG_STRONG
    DH:MODP_2048_224
        RNG:RNG_STRONG
    DH:MODP_2048_256
        RNG:RNG_STRONG
    DH:MODP_1536
        RNG:RNG_STRONG
    DH:MODP_1024
        RNG:RNG_STRONG
    DH:MODP_1024_160
        RNG:RNG_STRONG
    DH:MODP_768
        RNG:RNG_STRONG
    DH:MODP_CUSTOM
        RNG:RNG_STRONG
    PRIVKEY:RSA
    PRIVKEY_GEN:RSA
        RNG:RNG_TRUE
    PUBKEY:RSA
    PRIVKEY_SIGN:RSA_EMSA_PSS
        HASHER:HASH_SHA1 (soft)
        HASHER:HASH_SHA2_256 (soft)
        HASHER:HASH_SHA2_512 (soft)
        RNG:RNG_STRONG (soft)
        XOF:XOF_MGF1_SHA1 (soft)
        XOF:XOF_MGF1_SHA256 (soft)
        XOF:XOF_MGF1_SHA512 (soft)
    PRIVKEY_SIGN:RSA_EMSA_PKCS1_NULL
    PRIVKEY_SIGN:RSA_EMSA_PKCS1_SHA2_224
        HASHER:HASH_SHA2_224
    PRIVKEY_SIGN:RSA_EMSA_PKCS1_SHA2_256
        HASHER:HASH_SHA2_256
    PRIVKEY_SIGN:RSA_EMSA_PKCS1_SHA2_384
        HASHER:HASH_SHA2_384
    PRIVKEY_SIGN:RSA_EMSA_PKCS1_SHA2_512
        HASHER:HASH_SHA2_512
    PRIVKEY_SIGN:RSA_EMSA_PKCS1_SHA3_224 (not loaded)
        HASHER:HASH_SHA3_224
    PRIVKEY_SIGN:RSA_EMSA_PKCS1_SHA3_256 (not loaded)
        HASHER:HASH_SHA3_256
    PRIVKEY_SIGN:RSA_EMSA_PKCS1_SHA3_384 (not loaded)
        HASHER:HASH_SHA3_384
    PRIVKEY_SIGN:RSA_EMSA_PKCS1_SHA3_512 (not loaded)
        HASHER:HASH_SHA3_512
    PRIVKEY_SIGN:RSA_EMSA_PKCS1_SHA1
        HASHER:HASH_SHA1
    PRIVKEY_SIGN:RSA_EMSA_PKCS1_MD5 (not loaded)
        HASHER:HASH_MD5
    PUBKEY_VERIFY:RSA_EMSA_PSS
        HASHER:HASH_SHA1 (soft)
        HASHER:HASH_SHA2_256 (soft)
        HASHER:HASH_SHA2_512 (soft)
        XOF:XOF_MGF1_SHA1 (soft)
        XOF:XOF_MGF1_SHA256 (soft)
        XOF:XOF_MGF1_SHA512 (soft)
    PUBKEY_VERIFY:RSA_EMSA_PKCS1_NULL
    PUBKEY_VERIFY:RSA_EMSA_PKCS1_SHA2_224
        HASHER:HASH_SHA2_224
    PUBKEY_VERIFY:RSA_EMSA_PKCS1_SHA2_256
        HASHER:HASH_SHA2_256
    PUBKEY_VERIFY:RSA_EMSA_PKCS1_SHA2_384
        HASHER:HASH_SHA2_384
    PUBKEY_VERIFY:RSA_EMSA_PKCS1_SHA2_512
        HASHER:HASH_SHA2_512
    PUBKEY_VERIFY:RSA_EMSA_PKCS1_SHA3_224 (not loaded)
        HASHER:HASH_SHA3_224
    PUBKEY_VERIFY:RSA_EMSA_PKCS1_SHA3_256 (not loaded)
        HASHER:HASH_SHA3_256
    PUBKEY_VERIFY:RSA_EMSA_PKCS1_SHA3_384 (not loaded)
        HASHER:HASH_SHA3_384
    PUBKEY_VERIFY:RSA_EMSA_PKCS1_SHA3_512 (not loaded)
        HASHER:HASH_SHA3_512
    PUBKEY_VERIFY:RSA_EMSA_PKCS1_SHA1
        HASHER:HASH_SHA1
    PUBKEY_VERIFY:RSA_EMSA_PKCS1_MD5 (not loaded)
        HASHER:HASH_MD5
    PRIVKEY_DECRYPT:ENCRYPT_RSA_PKCS1
    PUBKEY_ENCRYPT:ENCRYPT_RSA_PKCS1
        RNG:RNG_WEAK
x509:
    CERT_ENCODE:X509
        HASHER:HASH_SHA1
    CERT_DECODE:X509
        HASHER:HASH_SHA1
        PUBKEY:ANY
    CERT_ENCODE:X509_AC
    CERT_DECODE:X509_AC
    CERT_ENCODE:X509_CRL
    CERT_DECODE:X509_CRL
    CERT_ENCODE:OCSP_REQUEST
        HASHER:HASH_SHA1
        RNG:RNG_WEAK
    CERT_DECODE:OCSP_RESPONSE
    CERT_ENCODE:PKCS10_REQUEST
    CERT_DECODE:PKCS10_REQUEST
curl:
    FETCHER:file://
    FETCHER:ftp://
    FETCHER:http://
    FETCHER:https:// (not loaded)
        CUSTOM:openssl-threading
revocation:
    CUSTOM:revocation
        CERT_ENCODE:OCSP_REQUEST (soft)
        CERT_DECODE:OCSP_RESPONSE (soft)
        CERT_DECODE:X509_CRL (soft)
        CERT_DECODE:X509 (soft)
        FETCHER:(null) (soft)
hmac:
    PRF:PRF_HMAC_SHA1
        HASHER:HASH_SHA1
    PRF:PRF_HMAC_MD5 (not loaded)
        HASHER:HASH_MD5
    PRF:PRF_HMAC_SHA2_256
        HASHER:HASH_SHA2_256
    PRF:PRF_HMAC_SHA2_384
        HASHER:HASH_SHA2_384
    PRF:PRF_HMAC_SHA2_512
        HASHER:HASH_SHA2_512
    SIGNER:HMAC_SHA1_96
        HASHER:HASH_SHA1
    SIGNER:HMAC_SHA1_128
        HASHER:HASH_SHA1
    SIGNER:HMAC_SHA1_160
        HASHER:HASH_SHA1
    SIGNER:HMAC_MD5_96 (not loaded)
        HASHER:HASH_MD5
    SIGNER:HMAC_MD5_128 (not loaded)
        HASHER:HASH_MD5
    SIGNER:HMAC_SHA2_256_128
        HASHER:HASH_SHA2_256
    SIGNER:HMAC_SHA2_256_256
        HASHER:HASH_SHA2_256
    SIGNER:HMAC_SHA2_384_192
        HASHER:HASH_SHA2_384
    SIGNER:HMAC_SHA2_384_384
        HASHER:HASH_SHA2_384
    SIGNER:HMAC_SHA2_512_256
        HASHER:HASH_SHA2_512
    SIGNER:HMAC_SHA2_512_512
        HASHER:HASH_SHA2_512
stroke:
    CUSTOM:stroke
        CUSTOM:counters (soft)
        PRIVKEY:RSA (soft)
        PRIVKEY:ECDSA (soft)
        PRIVKEY:DSA (soft)
        PRIVKEY:BLISS (soft)
        PRIVKEY:ED25519 (soft)
        PRIVKEY:ED448 (soft)
        CERT_DECODE:ANY (soft)
        CERT_DECODE:X509 (soft)
        CERT_DECODE:X509_CRL (soft)
        CERT_DECODE:X509_AC (soft)
        CERT_DECODE:PUBKEY (soft)
kernel-netlink:
    CUSTOM:kernel-ipsec
    CUSTOM:kernel-net
socket-default:
    CUSTOM:socket
		CUSTOM:kernel-ipsec (soft)`

const IpsecCertsExample2 = `List of X.509 CA Certificates

  subject:  "C=RU, O=O, OU=OU, CN=strongswan Root CA"
  issuer:   "C=RU, O=O, OU=OU, CN=strongswan Root CA"
  validity:  not before Sep 20 12:30:48 2019, ok
             not after  Sep 19 12:30:48 2022, ok (expires in 1094 days)
  serial:    0d:e3:aa:ec:85:06:90:22
  flags:     CA CRLSign self-signed 
  subjkeyId: 48:bb:04:8f:d7:52:8f:7b:30:b4:04:4f:2b:c8:54:59:a9:08:a1:0e
  pubkey:    RSA 2048 bits
  keyid:     0a:21:9a:36:bf:c9:2f:92:9e:ec:22:b7:06:6d:12:c7:ac:be:15:22
  subjkey:   48:bb:04:8f:d7:52:8f:7b:30:b4:04:4f:2b:c8:54:59:a9:08:a1:0e

List of registered IKE algorithms:

  encryption: AES_CBC[aesni] AES_CTR[aesni] RC2_CBC[rc2] 3DES_CBC[openssl] CAMELLIA_CBC[openssl] CAST_CBC[openssl]
              BLOWFISH_CBC[openssl] DES_CBC[openssl] DES_ECB[openssl] NULL[openssl]
  integrity:  AES_XCBC_96[aesni] AES_CMAC_96[aesni] HMAC_MD5_96[openssl] HMAC_MD5_128[openssl] HMAC_SHA1_96[openssl]
              HMAC_SHA1_128[openssl] HMAC_SHA1_160[openssl] HMAC_SHA2_256_128[op`

func TestIpsecCertsReader(t *testing.T) {
	icr := &IpsecCertsReader{}

	reader := bytes.NewBufferString(IpsecCertsExample1)
	certs, err := icr.Read(reader)
	if err != nil {
		t.Error("For err expected", nil, "returned", err)
		t.FailNow()
	}
	if len(certs) != 3 {
		t.Error("For len(certs) expected", 3, "returned", len(certs))
		t.FailNow()
	}

	reader = bytes.NewBufferString(IpsecCertsExample2)
	certs, err = icr.Read(reader)
	if err != nil {
		t.Error("For err expected", nil, "returned", err)
		t.FailNow()
	}
	if len(certs) != 1 {
		t.Error("For len(certs) expected", 1, "returned", len(certs))
		t.FailNow()
	}
}
