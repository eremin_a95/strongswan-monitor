package main

import (
	"fmt"
	"time"

	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
	"gitlab.com/eremin_a95/strongswan-monitor/strongswan"
)

// StatsPanel is type which represents statistics panel
type StatsPanel struct {
	Panel
	AtomicActivator
	Mutable
	summary              *widgets.Paragraph
	logs                 *widgets.List
	bIO, pIO             *widgets.SparklineGroup
	bIn, bOut, pIn, pOut *widgets.Sparkline
	bInArray, bOutArray  [1024]float64
	bInMax, bOutMax      float64
	bInLast, bOutLast    strongswan.ByteNum
	pInArray, pOutArray  [1024]float64
	pInMax, pOutMax      float64
	pInLast, pOutLast    uint64
	total                int
	initialState         int
	lastIO               time.Time
}

// HandleUIEvent implements interface UIEventHandler
func (sp *StatsPanel) HandleUIEvent(e ui.Event) {
	return
}

// MaxFloat64 finds index and value of maximum element in float64-slice
func MaxFloat64(slice []float64) (int, float64) {
	maxVal := slice[0]
	maxIndex := 0
	for i, v := range slice {
		if v > maxVal {
			maxVal = v
			maxIndex = i
		}
	}
	return maxIndex, maxVal
}

// Mutate reimplements Mutate method from Mutable interface
func (sp *StatsPanel) Mutate(v int) {
	switch v {
	case 0:
		info.RLock()
		var bIn, bOut strongswan.ByteNum
		var pIn, pOut uint64
		sp.total = 0
		for _, c := range info.Connections {
			for _, sa := range c.SAs {
				if sa.IKESA.Established {
					sp.total++
					for i := range sa.ChildSAs {
						bIn += sa.ChildSAs[i].BytesIn
						bOut += sa.ChildSAs[i].BytesOut
						pIn += sa.ChildSAs[i].PktsIn
						pOut += sa.ChildSAs[i].PktsOut
					}
				}
			}
		}
		since := info.Since
		info.RUnlock()
		sp.summary.Text = fmt.Sprintf("Uptime: %v (since %s)\nIKE SAs: %v total, %v connecting\nTotal in: %s (%v packets)\nTotal out: %s (%v packets)", time.Now().Sub(since).Round(time.Second), since.Format("15:04:05 02 Jan 2006"), sp.total, 0, bIn, pIn, bOut, pOut)
		leftLen := sp.bIO.GetRect().Max.Sub(sp.bIO.GetRect().Min).X - 2
		rightLen := sp.pIO.GetRect().Max.Sub(sp.pIO.GetRect().Min).X - 2
		var bInDif, bOutDif, pInDif, pOutDif float64
		if bIn > sp.bInLast {
			bInDif = float64(bIn - sp.bInLast)
		}
		if bOut > sp.bOutLast {
			bOutDif = float64(bOut - sp.bOutLast)
		}
		if pIn > sp.pInLast {
			pInDif = float64(pIn - sp.pInLast)
		}
		if pOut > sp.pOutLast {
			pOutDif = float64(pOut - sp.pOutLast)
		}
		sp.bInLast = bIn
		sp.bOutLast = bOut
		sp.pInLast = pIn
		sp.pOutLast = pOut
		if sp.initialState > 0 {
			sp.initialState--
			return
		}
		if cap(sp.bIn.Data) == len(sp.bIn.Data) {
			copy(sp.bInArray[0:leftLen], sp.bInArray[1024-leftLen:1024])
			sp.bIn.Data = sp.bInArray[0:leftLen]
		}
		if cap(sp.bOut.Data) == len(sp.bOut.Data) {
			copy(sp.bOutArray[0:leftLen], sp.bOutArray[1024-leftLen:1024])
			sp.bOut.Data = sp.bOutArray[0:leftLen]
		}
		if cap(sp.pIn.Data) == len(sp.pIn.Data) {
			copy(sp.pInArray[0:rightLen], sp.pInArray[1024-rightLen:1024])
			sp.pIn.Data = sp.pInArray[0:rightLen]
		}
		if cap(sp.pOut.Data) == len(sp.pOut.Data) {
			copy(sp.pOutArray[0:rightLen], sp.pOutArray[1024-rightLen:1024])
			sp.pOut.Data = sp.pOutArray[0:rightLen]
		}
		sp.bIn.Data = append(sp.bIn.Data[1:leftLen], bInDif)
		sp.bOut.Data = append(sp.bOut.Data[1:leftLen], bOutDif)
		sp.pIn.Data = append(sp.pIn.Data[1:rightLen], pInDif)
		sp.pOut.Data = append(sp.pOut.Data[1:rightLen], pOutDif)
		_, sp.bIn.MaxVal = MaxFloat64(sp.bIn.Data)
		_, sp.bOut.MaxVal = MaxFloat64(sp.bOut.Data)
		_, sp.pIn.MaxVal = MaxFloat64(sp.pIn.Data)
		_, sp.pOut.MaxVal = MaxFloat64(sp.pOut.Data)
		sp.bIn.Title = fmt.Sprintf("In, Max %s/sec", strongswan.ByteNum(sp.bIn.MaxVal/float64(time.Now().Sub(sp.lastIO)/time.Second)).BitNum())
		sp.bOut.Title = fmt.Sprintf("Out, Max %s/sec", strongswan.ByteNum(sp.bOut.MaxVal/float64(time.Now().Sub(sp.lastIO)/time.Second)).BitNum())
		sp.pIn.Title = fmt.Sprintf("In, Max %v packets/sec", sp.pIn.MaxVal/float64(time.Now().Sub(sp.lastIO)/time.Second))
		sp.pOut.Title = fmt.Sprintf("Out, Max %v packets/sec", sp.pOut.MaxVal/float64(time.Now().Sub(sp.lastIO)/time.Second))
		sp.lastIO = time.Now()
	case 1:
		info.RLock()
		since := info.Since
		info.RUnlock()
		sp.summary.Text = fmt.Sprintf("Uptime: %v (since %s)\nIKE SAs: %v total, %v connecting\nTotal in: %s (%v packets)\nTotal out: %s (%v packets)", time.Now().Sub(since).Round(time.Second), since.Format("15:04:05 02 Jan 2006"), sp.total, 0, sp.bInLast, sp.pInLast, sp.bOutLast, sp.pOutLast)
	}
	if sp.IsActivated() {
		sp.Mutable.Mutate(v)
	}
}

// NewStatsPanel creates new StatsPanel with statistics widgets
func NewStatsPanel() *StatsPanel {
	sp := &StatsPanel{
		Mutable:      NewSimpleMutable(),
		Panel:        *NewPanel("Statistics"),
		summary:      widgets.NewParagraph(),
		logs:         widgets.NewList(),
		bIn:          widgets.NewSparkline(),
		bOut:         widgets.NewSparkline(),
		pIn:          widgets.NewSparkline(),
		pOut:         widgets.NewSparkline(),
		initialState: 3,
	}
	termX, termY := ui.TerminalDimensions()
	sp.SetRect(0, 0, termX, termY)
	sp.TitleStyle = ui.NewStyle(ui.ColorClear, ui.ColorClear, ui.ModifierBold)
	sp.BorderStyle = ui.NewStyle(ui.ColorClear, ui.ColorClear, ui.ModifierBold)
	//sp.summary.Border = false
	sp.summary.SetRect(0, 0, termX, 6)
	sp.summary.Title = "Summary"
	sp.bIn.Title = "In, Max 0 bytes/sec"
	sp.bIn.Data = sp.bInArray[:64]
	sp.bIn.LineColor = ui.ColorBlue
	sp.bOut.Title = "Out, Max 0 bytes/sec"
	sp.bOut.Data = sp.bOutArray[:64]
	sp.bOut.LineColor = ui.ColorGreen
	sp.bIO = widgets.NewSparklineGroup(sp.bIn, sp.bOut)
	sp.bIO.Title = "Bytes In/Out"
	sp.bIO.SetRect(0, 6, termX/2-1, termY)
	sp.pIn.Title = "In, Max 0 packets/sec"
	sp.pIn.Data = sp.pInArray[:64]
	sp.pIn.LineColor = ui.ColorBlue
	sp.pOut.Title = "Out, Max 0 packets/sec"
	sp.pOut.Data = sp.pOutArray[:64]
	sp.pOut.LineColor = ui.ColorGreen
	sp.pIO = widgets.NewSparklineGroup(sp.pIn, sp.pOut)
	sp.pIO.Title = "Packets In/Out"
	sp.pIO.SetRect(termX/2-1, 6, termX, termY)
	sp.Add(sp.summary, sp.bIO, sp.pIO)
	sp.lastIO = time.Now()

	connsCh := info.ConnsRoutine.RegObserver()
	uptimeCh := info.UptimeRoutine.RegObserver()

	go func() {
		for <-connsCh == 0 {
			sp.Mutate(0)
		}
	}()

	go func() {
		for <-uptimeCh == 0 {
			sp.Mutate(1)
		}
	}()

	return sp
}
