package strongswan

import (
	"sync"
	"time"
)

// Info is type to store all StrongSwan daemon info
type Info struct {
	sync.RWMutex
	Uptime            time.Duration
	Since             time.Time
	Connections       []*Connection
	Pools             []Pool
	Certs             []*Cert
	UptimeRoutine     *PeriodicObservableRoutine
	ConnsRoutine      *PeriodicObservableRoutine
	CertsRoutine      *PeriodicObservableRoutine
	PoolsRoutine      *PeriodicObservableRoutine
	routines          [4]*PeriodicObservableRoutine
	uptimeGetter      *UptimeGetter
	connectionsGetter *ConnectionsGetter
	poolsGetter       *PoolsGetter
	certsGetter       *CertsGetter
}

// Start starts all Info's goroutines
func (info *Info) Start() {
	for _, r := range info.routines {
		r.Go()
	}
}

// Stop stops all Info's goroutines
func (info *Info) Stop() {
	for _, r := range info.routines {
		r.stopChan <- 0
	}
}

// NewInfo creates new instance of Info structure
func NewInfo(uptimePeriod, poolsPeriod, connectionsPeriod, certsPeriod time.Duration) (*Info, error) {
	info := &Info{
		Connections: make([]*Connection, 0),
		Pools:       make([]Pool, 0),
		Certs:       make([]*Cert, 0),
	}
	var err error
	info.uptimeGetter, err = NewUptimeGetter()
	if err != nil {
		return nil, err
	}
	info.connectionsGetter, err = NewConnectionsGetter()
	if err != nil {
		return nil, err
	}
	info.poolsGetter, err = NewPoolsGetter()
	if err != nil {
		return nil, err
	}
	info.certsGetter, err = NewCertsGetter()
	if err != nil {
		return nil, err
	}

	info.routines[0] = NewPeriodicObservableRoutine(uptimePeriod, func(por *PeriodicObservableRoutine) {
		if por.local[0].(time.Duration) > 0 {
			por.local[0] = por.local[0].(time.Duration) - por.period
			info.Lock()
			info.Uptime += por.period
			info.Unlock()
			por.Notify(0)
			return
		}
		por.local[0] = time.Second * 5
		_, since, err := info.uptimeGetter.GetUptime()
		if err != nil {
			return
		}
		uptime := time.Now().Sub(since)
		info.Lock()
		info.Uptime = uptime
		info.Since = since
		info.Unlock()
		por.Notify(0)
	})
	info.routines[0].local = make([]interface{}, 1)
	info.routines[0].local[0] = time.Duration(0)
	info.UptimeRoutine = info.routines[0]

	info.routines[1] = NewPeriodicObservableRoutine(connectionsPeriod, func(por *PeriodicObservableRoutine) {
		conns, err := info.connectionsGetter.GetConns()
		if err != nil {
			return
		}
		info.Lock()
		info.Connections = conns
		info.Unlock()
		por.Notify(0)
	})
	info.ConnsRoutine = info.routines[1]

	info.routines[2] = NewPeriodicObservableRoutine(poolsPeriod, func(por *PeriodicObservableRoutine) {
		pools, err := info.poolsGetter.GetPools()
		if err != nil {
			return
		}
		info.Lock()
		info.Pools = pools
		info.Unlock()
		por.Notify(0)
	})
	info.PoolsRoutine = info.routines[2]

	info.routines[3] = NewPeriodicObservableRoutine(certsPeriod, func(por *PeriodicObservableRoutine) {
		certs, err := info.certsGetter.GetCerts()
		if err != nil {
			return
		}
		info.Lock()
		info.Certs = certs
		info.Unlock()
		por.Notify(0)
	})
	info.CertsRoutine = info.routines[3]

	info.Start()

	return info, nil
}
