package main

import (
	"fmt"

	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
)

// ConnectionsPanel is type which represents Connections panel
type ConnectionsPanel struct {
	Panel
	AtomicActivator
	Mutable
	connections *widgets.Tree
	nodes       []*widgets.TreeNode
}

// HandleUIEvent implements interface UIEventHandler
func (cp *ConnectionsPanel) HandleUIEvent(e ui.Event) {
	switch e.ID {
	case "<Enter>":
		cp.connections.ToggleExpand()
	case "e":
		cp.connections.Expand()
	case "c":
		cp.connections.Collapse()
	case "E":
		cp.connections.ExpandAll()
	case "C":
		cp.connections.CollapseAll()
	case "<Down>":
		if len(cp.nodes) > 0 {
			cp.connections.ScrollDown()
		}
	case "<Up>":
		if len(cp.nodes) > 0 {
			cp.connections.ScrollUp()
		}
	default:
		return
	}
}

// Mutate reimplements Mutate method from Mutable interface
func (cp *ConnectionsPanel) Mutate(v int) {
	if cp.IsActivated() {
		info.RLock()
		if len(cp.nodes) > len(info.Connections) {
			cp.nodes = cp.nodes[:len(info.Connections)]
			cp.connections.ScrollTop()
		}
		for i, v := range info.Connections {
			if len(cp.nodes) <= i {
				cp.nodes = append(cp.nodes, nil)
			}
			if cp.nodes[i] == nil {
				cp.nodes[i] = new(widgets.TreeNode)
				cp.nodes[i].Nodes = make([]*widgets.TreeNode, 3)
				cp.nodes[i].Nodes[0] = new(widgets.TreeNode)
				cp.nodes[i].Nodes[0].Nodes = make([]*widgets.TreeNode, 2)
				cp.nodes[i].Nodes[0].Nodes[0] = new(widgets.TreeNode)
				cp.nodes[i].Nodes[0].Nodes[1] = new(widgets.TreeNode)
				cp.nodes[i].Nodes[1] = new(widgets.TreeNode)
				cp.nodes[i].Nodes[1].Nodes = make([]*widgets.TreeNode, 2)
				cp.nodes[i].Nodes[1].Nodes[0] = new(widgets.TreeNode)
				cp.nodes[i].Nodes[1].Nodes[1] = new(widgets.TreeNode)
				cp.nodes[i].Nodes[2] = new(widgets.TreeNode)
				cp.nodes[i].Nodes[2].Nodes = make([]*widgets.TreeNode, len(v.Children))
				for j := range v.Children {
					cp.nodes[i].Nodes[2].Nodes[j] = new(widgets.TreeNode)
				}
			}
			cp.nodes[i].Value = NodeString(fmt.Sprintf("%s: %s <---> %s (%s)", v.Name, v.LocalAddr, v.RemoteAddr, v.Version))
			cp.nodes[i].Nodes[0].Value = NodeString(fmt.Sprintf("Local %s", v.LocalAddr))
			cp.nodes[i].Nodes[0].Nodes[0].Value = NodeString(fmt.Sprintf("Auth: %s", v.LocalAuth))
			if len(v.LocalID) == 0 {
				cp.nodes[i].Nodes[0].Nodes = cp.nodes[i].Nodes[0].Nodes[:1]
			} else {
				if len(cp.nodes[i].Nodes[0].Nodes) == 1 {
					cp.nodes[i].Nodes[0].Nodes = cp.nodes[i].Nodes[0].Nodes[:2]
				}
				cp.nodes[i].Nodes[0].Nodes[1].Value = NodeString(fmt.Sprintf("ID: %s", v.LocalID))
			}
			cp.nodes[i].Nodes[1].Value = NodeString(fmt.Sprintf("Remote %s", v.RemoteAddr))
			cp.nodes[i].Nodes[1].Nodes[0].Value = NodeString(fmt.Sprintf("Auth: %s", v.RemoteAuth))
			if len(v.RemoteID) == 0 {
				cp.nodes[i].Nodes[1].Nodes = cp.nodes[i].Nodes[1].Nodes[:1]
			} else {
				if len(cp.nodes[i].Nodes[1].Nodes) == 1 {
					cp.nodes[i].Nodes[1].Nodes = cp.nodes[i].Nodes[1].Nodes[:2]
				}
				cp.nodes[i].Nodes[1].Nodes[1].Value = NodeString(fmt.Sprintf("ID: %s", v.RemoteID))
			}
			cp.nodes[i].Nodes[2].Value = NodeString("Child")
			if len(cp.nodes[i].Nodes[2].Nodes) > len(v.Children) {
				cp.nodes[i].Nodes[2].Nodes = cp.nodes[i].Nodes[2].Nodes[:len(v.Children)]
			}
			for j, child := range v.Children {
				if len(cp.nodes[i].Nodes[2].Nodes) <= j {
					cp.nodes[i].Nodes[2].Nodes = append(cp.nodes[i].Nodes[2].Nodes, new(widgets.TreeNode))
				}
				cp.nodes[i].Nodes[2].Nodes[j].Value = NodeString(child.String())
			}
		}
		info.RUnlock()
		cp.connections.SetNodes(cp.nodes)
		cp.Mutable.Mutate(v)
	}
}

// NodeString is type for inserting into widgets.TreeNode
type NodeString string

func (ns NodeString) String() string {
	return string(ns) + " "
}

// NewConnectionsPanel creates new ConnectionsPanel with list of connections
func NewConnectionsPanel() *ConnectionsPanel {
	cp := &ConnectionsPanel{
		Panel:       *NewPanel("Connections"),
		Mutable:     NewSimpleMutable(),
		connections: widgets.NewTree(),
		nodes:       make([]*widgets.TreeNode, 0, 4),
	}
	termX, termY := ui.TerminalDimensions()
	cp.SetRect(0, 0, termX, termY)
	cp.TitleStyle = ui.NewStyle(ui.ColorClear, ui.ColorClear, ui.ModifierBold)
	cp.BorderStyle = ui.NewStyle(ui.ColorClear, ui.ColorClear, ui.ModifierBold)
	cp.connections.SetRect(0, 0, termX, termY)
	cp.connections.Border = false
	cp.connections.SelectedRowStyle = ui.NewStyle(ui.ColorClear, ui.ColorClear, ui.ModifierBold)
	cp.Add(cp.connections)

	ch := info.ConnsRoutine.RegObserver()

	go func() {
		for <-ch == 0 {
			cp.Mutate(0)
		}
	}()

	return cp
}
