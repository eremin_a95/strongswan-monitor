package strongswan

import (
	"sync"
	"time"
)

// Callback is type for callback functions
type Callback func(...interface{})

// Observable is interface for types who can
// register and unregister observers callbacks
type Observable interface {
	RegObserver() chan int
	UnregObserver(chan int)
	Notify(int)
}

// PeriodicObservableRoutine is type which is observable and can start
// goroutine with periodic-executable function
type PeriodicObservableRoutine struct {
	mutex    sync.Mutex
	stopChan chan int
	period   time.Duration
	routine  func(*PeriodicObservableRoutine)
	token    int
	obs      []chan int
	local    []interface{}
}

// NewPeriodicObservableRoutine returns pointer to
// new instance of PeriodicObservableRoutine
func NewPeriodicObservableRoutine(period time.Duration, routine func(*PeriodicObservableRoutine)) *PeriodicObservableRoutine {
	return &PeriodicObservableRoutine{
		stopChan: make(chan int),
		period:   period,
		routine:  routine,
		token:    0,
		obs:      make([]chan int, 0, 4),
		local:    make([]interface{}, 0, 4),
	}
}

// RegObserver implements Observable interface
func (por *PeriodicObservableRoutine) RegObserver() chan int {
	por.mutex.Lock()
	ch := make(chan int)
	por.obs = append(por.obs, ch)
	por.mutex.Unlock()
	return ch
}

// UnregObserver implements Observable interface
func (por *PeriodicObservableRoutine) UnregObserver(ch chan int) {
	por.mutex.Lock()
	defer por.mutex.Unlock()
	for i := range por.obs {
		if por.obs[i] == ch {
			por.obs = append(por.obs[:i], por.obs[i+1:]...)
			return
		}
	}
}

// Notify implements Observable interface
func (por *PeriodicObservableRoutine) Notify(val int) {
	por.mutex.Lock()
	for i := range por.obs {
		por.obs[i] <- val
	}
	defer por.mutex.Unlock()
}

// Go starts gorotine which runs PeriodicObservableRoutine.routine every
// PeriodicObservableRoutine.period and can be stopped by writing some data to
// PeriodicObservableRoutine.stopChan
func (por *PeriodicObservableRoutine) Go() {
	go func() {
		for ; ; time.Sleep(por.period) {
			select {
			case <-por.stopChan:
				return
			default:
				por.routine(por)
			}
		}
	}()
}
