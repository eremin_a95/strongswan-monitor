package main

import (
	"testing"

	"github.com/gizak/termui/v3/widgets"
)

func TestPanel(t *testing.T) {
	panel := NewPanel("Test")
	panel.SetRect(0, 0, 10, 10)
	p0 := widgets.NewParagraph()
	p0.SetRect(0, 0, 5, 5)
	p1 := widgets.NewParagraph()
	p1.SetRect(5, 5, 10, 10)
	panel.Add(p0, p1)
	panel.SetRect(4, 4, 12, 12)

	c0 := panel.contents[0]
	c1 := panel.contents[1]
	if c0.GetRect().Min.X != 5 {
		t.Error("For", c0, "Min.X expected", 5, "got", c0.GetRect().Min.X)
	}
	if c0.GetRect().Min.Y != 5 {
		t.Error("For", c0, "Min.Y expected", 5, "got", c0.GetRect().Min.Y)
	}
	if c0.GetRect().Max.X != 10 {
		t.Error("For", c0, "Max.X expected", 10, "got", c0.GetRect().Max.X)
	}
	if c0.GetRect().Max.Y != 10 {
		t.Error("For", c0, "Max.Y expected", 10, "got", c0.GetRect().Max.Y)
	}

	if c1.GetRect().Min.X != 10 {
		t.Error("For", c1, "Min.X expected", 10, "got", c1.GetRect().Min.X)
	}
	if c1.GetRect().Min.Y != 10 {
		t.Error("For", c1, "Min.Y expected", 10, "got", c1.GetRect().Min.Y)
	}
	if c1.GetRect().Max.X != 11 {
		t.Error("For", c1, "Max.X expected", 11, "got", c1.GetRect().Max.X)
	}
	if c1.GetRect().Max.Y != 11 {
		t.Error("For", c1, "Max.Y expected", 11, "got", c1.GetRect().Max.Y)
	}
}
