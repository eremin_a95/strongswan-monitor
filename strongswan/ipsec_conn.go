package strongswan

import (
	"fmt"
	"io"
	"strconv"
	"strings"
)

// IpsecConnReader is type to Read slice of Connections
// with `ipsec statusall` format
type IpsecConnReader struct {
	buf [4096]byte
}

func readConnections(str string) ([]*Connection, error) {
	lines := strings.Split(str, "\n")
	conns := make([]*Connection, 0, len(lines)/3)
	oldName := ""
	copied := false
	i := -1
	for _, line := range lines {
		splitted := strings.SplitN(line, ":  ", 2)
		if oldName != splitted[0] {
			conns = append(conns, &Connection{SAs: make([]*SA, 0, 4)})
			i++
			if i > 0 {
				*conns[i] = *conns[i-1] // In case of omitting params
				conns[i].Children = nil
				conns[i].SAs = make([]*SA, 0, 4)
				conns[i].RemoteAuth = make([]AuthType, 0, 2)
				conns[i].RemoteAuth = append(conns[i].RemoteAuth, conns[i-1].RemoteAuth...)
				copied = true
			} else {
				conns[i].RemoteAuth = make([]AuthType, 0, 2)
			}
			conns[i].LocalAuth = make([]AuthType, 0, 2)
			conns[i].Name = strings.TrimSpace(splitted[0])
			oldName = splitted[0]
		}
		splitted = strings.Split(line, "local:  ")
		if len(splitted) > 1 {
			splitted = strings.Split(splitted[1], "uses ")
			if len(splitted[0]) >= 2 {
				trimmed := strings.TrimSpace(splitted[0])
				conns[i].LocalID = strings.Split(trimmed[1:len(trimmed)-1], ",")
			}
			conns[i].LocalAuth = append(conns[i].LocalAuth, ParseAuthType(splitted[1]))
			continue
		}
		splitted = strings.Split(line, "cert:  ")
		if len(splitted) > 1 {
			// Read something???
			continue
		}
		splitted = strings.Split(line, "remote: ")
		if len(splitted) > 1 {
			splitted = strings.Split(splitted[1], "uses ")
			if len(splitted[0]) >= 2 {
				trimmed := strings.TrimSpace(splitted[0])
				conns[i].RemoteID = strings.Split(trimmed[1:len(trimmed)-1], ",")
			}
			if copied {
				conns[i].RemoteAuth = conns[i].RemoteAuth[:0:0]
			}
			conns[i].RemoteAuth = append(conns[i].RemoteAuth, ParseAuthType(splitted[1]))
			copied = false
			continue
		}
		splitted = strings.Split(line, "crl:   ")
		if len(splitted) > 1 {
			// Read something???
			continue
		}
		splitted = strings.Split(line, "ca:    ")
		if len(splitted) > 1 {
			// Read something???
			continue
		}
		splitted = strings.Split(line, "child:  ")
		if len(splitted) > 1 {
			if conns[i].Children == nil {
				conns[i].Children = make([]Child, 0, 2)
			}
			splitted = strings.Split(splitted[1], " ")
			mode := ParseConnectionMode(splitted[3])
			conns[i].Children = append(conns[i].Children, Child{
				Local:  strings.Split(splitted[0], ","),
				Remote: strings.Split(splitted[2], ","),
				Mode:   mode,
			})
			continue
		}

		result := strings.Split(line, ":  ")
		if len(result) < 2 {
			return nil, fmt.Errorf("Incorrect format of incoming data: missing \":  \" splitter in %s", line)
		}
		splitted = strings.Split(result[1], "  ")
		if len(splitted) < 2 {
			return nil, fmt.Errorf("Incorrect format of incoming data: missing \"  \" splitter in %s", result[1])
		}
		conns[i].Version = ParseIKEVersion(splitted[1])
		splitted = strings.Split(splitted[0], "...")
		if len(splitted) < 2 {
			return nil, fmt.Errorf("Incorrect format of incoming data: missing \"...\" splitter in %s", splitted[0])
		}
		conns[i].LocalAddr = strings.Split(splitted[0], ",")
		conns[i].RemoteAddr = strings.Split(splitted[1], ",")
	}
	return conns, nil
}

func readSAHead(sa *SA, line string) error {
	splitted := strings.Split(line, "...")
	if len(splitted) != 2 {
		return fmt.Errorf("Incorrect format of incoming data: missing \"...\" splitter in %s", line)
	}
	temp := strings.Split(splitted[0], "[")
	sa.IKESA.LocalAddr = temp[0]
	sa.IKESA.LocalID = strings.TrimSuffix(temp[1], "]")
	temp = strings.Split(splitted[1], "[")
	sa.IKESA.RemoteAddr = temp[0]
	sa.IKESA.RemoteID = strings.TrimSuffix(temp[1], "]")
	return nil
}

func readSAs(lines []string, conns []*Connection) ([]*Connection, error) {
	var sa *SA
	if len(lines) < 3 {
		return conns, nil
	}
	childSAIter := -1
	for i := 0; i < len(lines); i++ {
		if lines[i] == "" {
			continue
		}
		splitted := strings.SplitN(lines[i], ": ", 2)
		if len(splitted) != 2 {
			return nil, fmt.Errorf("Incorrect format of incoming data: missing \": \" splitter in \"%s\"", lines[i])
		}
		splitted[0] = strings.TrimSpace(splitted[0])
		splitted[1] = strings.TrimSpace(splitted[1])
		if sa == nil && !strings.HasPrefix(splitted[1], "ESTABLISHED") && !strings.HasPrefix(splitted[1], "DELETING") {
			continue
		}
		switch {
		case strings.HasPrefix(splitted[1], "ESTABLISHED"), strings.HasPrefix(splitted[1], "DELETING"):
			childSAIter = -1
			name := strings.TrimSpace(strings.Split(splitted[0], "[")[0])
			temp := strings.SplitN(splitted[1], ", ", 2)
			if len(temp) != 2 {
				return nil, fmt.Errorf("Incorrect format of incoming data: missing \", \" splitter in \"%s\"", splitted[1])
			}
			sa = new(SA)
			sa.ChildSAs = make([]ChildSA, 0, 4)
			if strings.HasPrefix(temp[0], "ESTABLISHED") {
				sa.IKESA.Established = true
				sa.IKESA.Alive = strings.TrimSuffix(strings.TrimPrefix(temp[0], "ESTABLISHED "), " ago")
			} else {
				sa.IKESA.Deleting = true
			}
			if err := readSAHead(sa, temp[1]); err != nil {
				return nil, err
			}
			for i := range conns {
				if conns[i].Name == name {
					conns[i].SAs = append(conns[i].SAs, sa)
					break
				}
			}
		case strings.HasPrefix(splitted[1], "IKEv2 SPIs"), strings.HasPrefix(splitted[1], "IKEv1 SPIs"):
			temp1 := strings.SplitN(splitted[1], " SPIs: ", 2)
			if len(temp1) != 2 {
				return nil, fmt.Errorf("Incorrect format of incoming data: missing \" SPIs: \" splitter in \"%s\"", splitted[1])
			}
			sa.IKESA.Version = ParseIKEVersion(temp1[0])
			temp1 = strings.Split(temp1[1], ", ")
			temp2 := strings.Split(temp1[0], " ")
			if len(temp2) != 2 {
				return nil, fmt.Errorf("Incorrect format of incoming data: missing \" \" splitter in %s", temp1[0])
			}
			sa.IKESA.InitiatorSPI = temp2[0][:16]
			sa.IKESA.ResponderSPI = temp2[1][:16]
			if len(temp2[0]) == 19 {
				sa.IKESA.IsInitiator = true
			}
			if len(temp1) == 2 {
				temp2 = strings.Split(temp1[1], " in ")
				if len(temp2) != 2 {
					return nil, fmt.Errorf("Incorrect format of incoming data: missing \" in \" splitter in %s", temp1[1])
				}
				sa.IKESA.Auth = ParseAuthType(temp2[0])
				sa.IKESA.UntilReauth = temp2[1]
			} else if len(temp1) != 1 {
				return nil, fmt.Errorf("Incorrect format of incoming data in \"%v\"", temp1)
			}
		case strings.HasPrefix(splitted[1], "IKE proposal"):
			temp := strings.Split(splitted[1], "IKE proposal: ")
			if len(temp) != 2 {
				return nil, fmt.Errorf("Incorrect format of incoming data: missing \"IKE proposal: \" splitter in \"%s\"", splitted[1])
			}
			sa.IKESA.Security = temp[1]
		case strings.HasPrefix(splitted[1], "Tasks active"):
			//TODO
		case strings.HasPrefix(splitted[1], "INSTALLED"):
			temp1 := strings.Split(splitted[1], ", ")
			if len(temp1) < 4 {
				return nil, fmt.Errorf("Incorrect format of incoming data in \"%s\"", splitted[1])
			}
			sa.ChildSAs = append(sa.ChildSAs, ChildSA{})
			childSAIter++
			sa.ChildSAs[childSAIter].Installed = true
			sa.ChildSAs[childSAIter].Mode = ParseConnectionMode(temp1[1])
			reqID, err := strconv.ParseInt(strings.Split(temp1[2], " ")[1], 10, 64)
			if err != nil {
				return nil, fmt.Errorf("Cannot parse ReqID from \"%s\": %s", temp1[2], err)
			}
			sa.ChildSAs[childSAIter].ReqID = reqID
			temp1 = strings.Split(temp1[3], " SPIs: ")
			if len(temp1) != 2 {
				return nil, fmt.Errorf("Incorrect format of incoming data: missing \" SPIs: \" splitter in \"%s\"", temp1[0])
			}
			temp2 := strings.Split(temp1[0], " ")
			sa.ChildSAs[childSAIter].Proto = ParseIPsecProto(temp2[0])
			if (len(temp2) == 3) && (temp2[2] == "UDP") {
				sa.ChildSAs[childSAIter].UDPEncaps = true
			}
			temp1 = strings.Split(temp1[1], " ")
			if len(temp1) != 2 {
				return nil, fmt.Errorf("Incorrect format of incoming data: missing \" \" splitter in %s", temp1[1])
			}
			sa.ChildSAs[childSAIter].InputSPI = temp1[0][:8]
			sa.ChildSAs[childSAIter].OutputSPI = temp1[1][:8]
		default:
			if sa == nil {
				continue
			}
			temp1 := strings.SplitN(splitted[1], ", ", 2)
			if len(temp1) != 2 {
				temp1 = strings.Split(splitted[1], " === ")
				if len(temp1) == 2 {
					sa.ChildSAs[childSAIter].LocalNet = temp1[0]
					sa.ChildSAs[childSAIter].RemoteNet = temp1[1]
				}
				continue
			}
			sa.ChildSAs[childSAIter].Security = temp1[0]
			temp1 = strings.SplitN(temp1[1], " bytes_i", 2)
			if len(temp1) != 2 {
				return nil, fmt.Errorf("Incorrect format of incoming data in \"%v\"", temp1)
			}
			bytesIn, err := strconv.ParseUint(temp1[0], 10, 64)
			if err != nil {
				return nil, fmt.Errorf("Cannot parse BytesIn from \"%s\": %s", temp1[0], err)
			}
			sa.ChildSAs[childSAIter].BytesIn = ByteNum(bytesIn)
			if bytesIn != 0 {
				temp1 = strings.SplitN(temp1[1], "(", 2)
				temp1 = strings.SplitN(temp1[1], ")", 2)
				temp2 := strings.Split(temp1[0], " ")
				pktsIn, err := strconv.ParseUint(temp2[0], 10, 64)
				if err != nil {
					return nil, fmt.Errorf("Cannot parse PktsIn from \"%s\": %s", temp2[0], err)
				}
				sa.ChildSAs[childSAIter].PktsIn = pktsIn
				sa.ChildSAs[childSAIter].LastInAgo = temp2[2]
			}

			temp1 = strings.SplitN(temp1[1], ", ", 2)
			temp1 = strings.SplitN(temp1[1], " bytes_o", 2)
			if len(temp1) != 2 {
				return nil, fmt.Errorf("Incorrect format of incoming data in \"%v\"", temp1)
			}
			bytesOut, err := strconv.ParseUint(temp1[0], 10, 64)
			if err != nil {
				return nil, fmt.Errorf("Cannot parse BytesOut from \"%s\": %s", temp1[0], err)
			}
			sa.ChildSAs[childSAIter].BytesOut = ByteNum(bytesOut)
			if bytesOut != 0 {
				temp1 = strings.Split(temp1[1], "(")
				temp1 = strings.Split(temp1[1], ")")
				temp2 := strings.Split(temp1[0], " ")
				pktsOut, err := strconv.ParseUint(temp2[0], 10, 64)
				if err != nil {
					return nil, fmt.Errorf("Cannot parse PktsOut from \"%s\": %s", temp2[0], err)
				}
				sa.ChildSAs[childSAIter].PktsOut = pktsOut
				sa.ChildSAs[childSAIter].LastOutAgo = temp2[2]
			}

			sa.ChildSAs[childSAIter].UntilRekey = strings.Split(temp1[1], " in ")[1]
		}
	}
	return conns, nil
}

func (icr *IpsecConnReader) Read(r io.Reader) ([]*Connection, error) {
	n, err := r.Read(icr.buf[:])
	if err != nil && err != io.EOF {
		return nil, err
	}
	sas := strings.Split(string(icr.buf[:n]), "Security Associations")[1]
	temp := string(icr.buf[:n])
	temp = strings.Split(temp, "\nRouted Connections:")[0]
	temp = strings.Split(temp, "\nShunted Connections:")[0]
	result := strings.Split(temp, "Connections:\n")
	if len(result) < 2 {
		return nil, fmt.Errorf("Incorrect format of incoming data: missing string \"Connections:\"")
	}
	temp = result[1]
	splitted := strings.Split(temp, "\nSecurity Associations")
	temp = splitted[0]
	if strings.HasPrefix(temp, "Security Associations") {
		return make([]*Connection, 0), nil
	}
	conns, err := readConnections(temp)
	if err != nil {
		return nil, err
	}
	lines := strings.Split(sas, "\n")
	lines = lines[1:]
	if len(lines) < 2 {
		//return conns, fmt.Errorf("Incorrect format of incoming data: missing string \"Security Associations (x up, y connecting):\"")
		return conns, nil
	}
	return readSAs(lines, conns)
}
