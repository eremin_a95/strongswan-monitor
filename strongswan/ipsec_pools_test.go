package strongswan

import (
	"bytes"
	"testing"
)

const IpsecPoolsExample1 = "Status of IKE charon daemon (strongSwan 5.8.0, Linux 5.1.3, x86_64):\n  uptime: 2 seconds, since May 20 09:44:52 2019\n  malloc: sbrk 3133440, mmap 0, used 2003936, free 1129504\n  worker threads: 11 of 16 idle, 5/0/0/0 working, job queue: 0/0/0/0, scheduled: 6\n  loaded plugins: charon random nonce aes sha1 sha2 pem pkcs1 curve25519 gmp x509 curl revocation hmac stroke kernel-netlink socket-default updown\nVirtual IP pools (size/online/offline):\n  10.3.0.0/28: 14/2/0\nListening IP addresses:\n  192.168.0.1\n  fec0::1\n10.1.0.1\n  fec1::1\nConnections:\n		rw:  192.168.0.1...%any  IKEv2\n		rw:   local:  [moon.strongswan.org] uses public key authentication\n		rw:    cert:  \"C=CH, O=strongSwan Project, CN=moon.strongswan.org\"\n		rw:   remote: uses public key authentication\n		rw:   child:  10.1.0.0/16 === dynamic TUNNEL\nSecurity Associations (2 up, 0 connecting):\n		rw[2]: ESTABLISHED 1 second ago, 192.168.0.1[moon.strongswan.org]...192.168.0.200[dave@strongswan.org]\n		rw[2]: IKEv2 SPIs: 43007f5e20587c64_i bd4253212c4703b5_r*, public key reauthentication in 54 minutes\n		rw[2]: IKE proposal: AES_CBC_128/HMAC_SHA2_256_128/PRF_HMAC_SHA2_256/CURVE_25519\n		rw{2}:  INSTALLED, TUNNEL, reqid 2, ESP SPIs: c7879e63_i cad59f8b_o\n		rw{2}:  AES_CBC_128/HMAC_SHA2_256_128, 84 bytes_i (1 pkt, 1s ago), 84 bytes_o (1 pkt, 1s ago), rekeying in 14 minutes\n		rw{2}:   10.1.0.0/16 === 10.3.0.2/32\n		rw[1]: ESTABLISHED 2 seconds ago, 192.168.0.1[moon.strongswan.org]...192.168.0.100[carol@strongswan.org]\n		rw[1]: IKEv2 SPIs: 644731b1e0447ae6_i a14e1d4b4415397e_r*, public key reauthentication in 55 minutes\n		rw[1]: IKE proposal: AES_CBC_128/HMAC_SHA2_256_128/PRF_HMAC_SHA2_256/CURVE_25519\n		rw{1}:  INSTALLED, TUNNEL, reqid 1, ESP SPIs: c838a5fe_i c705d783_o\n		rw{1}:  AES_CBC_128/HMAC_SHA2_256_128, 84 bytes_i (1 pkt, 1s ago), 84 bytes_o (1 pkt, 1s ago), rekeying in 14 minutes\n		rw{1}:   10.1.0.0/16 === 10.3.0.1/32"

const IpsecPoolsExample2 = "Status of IKE charon daemon (strongSwan 5.3.5, Linux 4.4.0-159-generic, x86_64):\n  uptime: 6 minutes, since Sep 02 03:56:40 2019\n  malloc: sbrk 1351680, mmap 0, used 314752, free 1036928\n  worker threads: 11 of 16 idle, 5/0/0/0 working, job queue: 0/0/0/0, scheduled: 0\n  loaded plugins: charon test-vectors aes rc2 sha1 sha2 md4 md5 random nonce x509 revocation constraints pubkey pkcs1 pkcs7 pkcs8 pkcs12 pgp dnskey sshkey pem openssl fips-prf gmp agent xcbc hmac gcm attr kernel-netlink resolve socket-default connmark stroke updown\nListening IP addresses:\n  192.168.0.201\nConnections:\nSecurity Associations (0 up, 0 connecting):\n  none\n"

func TestIpsecPoolsReader(t *testing.T) {
	reader := bytes.NewBufferString(IpsecPoolsExample1)
	ipr := &IpsecPoolsReader{}
	pools, err := ipr.Read(reader)
	if err != nil {
		t.Error("For err expected", nil, "returned", err)
		t.FailNow()
	}
	if len(pools) != 1 {
		t.Error("For len(pools) expected", 1, "returned", len(pools))
	}
	if pools[0].Addr != "10.3.0.0/28" {
		t.Error("For pools[0].Addr expected 10.3.0.0/28 returned", pools[0].Addr)
	}
	if pools[0].Size != 14 {
		t.Error("For pools[0].Size expected", 14, "returned", pools[0].Size)
	}
	if pools[0].On != 2 {
		t.Error("For pools[0].On expected", 2, "returned", pools[0].On)
	}
	if pools[0].Off != 0 {
		t.Error("For pools[0].Off expected", 0, "returned", pools[0].Off)
	}

	reader = bytes.NewBufferString(IpsecPoolsExample2)
	pools, err = ipr.Read(reader)
	if err != nil {
		t.Error("For err expected", nil, "returned", err)
		t.FailNow()
	}
	if len(pools) != 0 {
		t.Error("For len(pools) expected", 0, "returned", len(pools))
	}
}
